pub(crate) fn connect(mut config: crate::config::Config) -> Result<crate::oxy::Oxy, crate::Error> {
    config.prepare_secrets()?;
    let destination = match config.destination {
        Some(ref destination) => destination,
        None => {
            log::error!("No destination specified.");
            return Err(crate::Error::NoDestination);
        }
    };
    let socket = match std::net::TcpStream::connect(destination) {
        Ok(socket) => socket,
        Err(err) => {
            log::error!("Connect failed: {}", err);
            return Err(crate::Error::ConnectFailed);
        }
    };
    log::trace!(
        "Connection established to {}.",
        match socket.peer_addr() {
            Ok(x) => x.to_string(),
            Err(_) => "unknown".to_string(),
        }
    );
    let socket = match mio::net::TcpStream::from_stream(socket) {
        Ok(socket) => socket,
        Err(err) => {
            log::error!("Failed to mio-ize socket: {}", err);
            return Err(crate::Error::Mio(err));
        }
    };
    crate::oxy::Oxy::new(config, socket)
}
