#![allow(clippy::integer_arithmetic)] // This is only compile time arithmetic so this lint shouldn't actually trigger.

#[derive(bytepack::BytePack)]
pub struct ClientHello {
    pub symmetric_nonce: [u8; crate::crypto::secretbox::NONCEBYTES],
    pub symmetric_mac: [u8; crate::crypto::secretbox::MACBYTES],
    pub symmetric_ciphertext: [u8; <ClientHelloInner1 as bytepack::BytePack>::WIDTH],
}

const ASYMMETRIC_CIPHERTEXT_LEN: usize =
    <ClientHelloInner2 as bytepack::BytePack>::WIDTH + crate::crypto::sealedbox::SEALBYTES;

#[derive(bytepack::BytePack)]
pub struct ClientHelloInner1 {
    pub asymmetric_ciphertext: [u8; ASYMMETRIC_CIPHERTEXT_LEN],
}

#[derive(bytepack::BytePack)]
pub struct ClientHelloInner2 {
    pub client_pubkey: [u8; crate::crypto::box_::PUBLICKEYBYTES],
    pub asymmetric2_nonce: [u8; crate::crypto::box_::NONCEBYTES],
    pub asymmetric2_mac: [u8; crate::crypto::box_::MACBYTES],
    pub asymmetric2_ciphertext: [u8; <ClientHelloInner3 as bytepack::BytePack>::WIDTH],
}

#[derive(bytepack::BytePack)]
pub struct ClientHelloInner3 {
    pub clientsend_length_key: [u8; crate::crypto::stream::KEYBYTES],
    pub clientsend_length_nonce: [u8; crate::crypto::stream::NONCEBYTES],
    pub serversend_length_key: [u8; crate::crypto::stream::KEYBYTES],
    pub serversend_length_nonce: [u8; crate::crypto::stream::NONCEBYTES],
    pub clientsend_stream_key: [u8; crate::crypto::secretstream::KEYBYTES],
    pub clientsend_stream_header: [u8; crate::crypto::secretstream::HEADERBYTES],
    pub serversend_stream_key: [u8; crate::crypto::secretstream::KEYBYTES],
}

#[derive(num_derive::ToPrimitive, num_derive::FromPrimitive, Debug)]
pub enum MessageType {
    Null,
    Accept,
    Reject,
    SimpleCommand,
    SimpleOutput,
    TtyRequest,
    TtyData,
    TtyResize,
    BindRequest,
    ConnectRequest,
    BindConnection,
    ServerAcceptConnectionData,
    ServerConnectConnectionData,
    CloseConnect,
    CloseBind,
}

#[derive(serde_derive::Serialize, serde_derive::Deserialize, Default, Debug)]
pub struct TtyRequest {
    pub program: Option<String>,
    pub args: Vec<String>,
    pub cols: u16,
    pub rows: u16,
}

#[derive(serde_derive::Serialize, serde_derive::Deserialize, Default, Debug)]
pub struct TtyResize {
    pub cols: u16,
    pub rows: u16,
}

impl TypeTagged for TtyResize {
    const MESSAGE_TYPE: MessageType = MessageType::TtyResize;
}

impl TypeTagged for TtyRequest {
    const MESSAGE_TYPE: MessageType = MessageType::TtyRequest;
}

#[derive(serde_derive::Serialize, serde_derive::Deserialize, Debug)]
pub struct SimpleCommand {
    pub program: String,
    pub args: Vec<String>,
}

#[derive(serde_derive::Serialize, serde_derive::Deserialize)]
pub struct Reject(pub u64);

impl TypeTagged for Reject {
    const MESSAGE_TYPE: MessageType = MessageType::Reject;
}

pub(crate) trait TypeTagged {
    const MESSAGE_TYPE: MessageType;
}

impl TypeTagged for SimpleCommand {
    const MESSAGE_TYPE: MessageType = MessageType::SimpleCommand;
}

#[derive(serde_derive::Serialize, serde_derive::Deserialize)]
pub struct BindRequest {
    pub bind_spec: std::net::SocketAddr,
}

impl TypeTagged for BindRequest {
    const MESSAGE_TYPE: MessageType = MessageType::BindRequest;
}

#[derive(serde_derive::Serialize, serde_derive::Deserialize)]
pub struct ConnectRequest {
    pub destination: String,
}

impl TypeTagged for ConnectRequest {
    const MESSAGE_TYPE: MessageType = MessageType::ConnectRequest;
}

#[derive(serde_derive::Serialize, serde_derive::Deserialize)]
pub struct BindConnection {
    pub bind_reference: u64,
    pub peer_addr: std::net::SocketAddr,
}

impl TypeTagged for BindConnection {
    const MESSAGE_TYPE: MessageType = MessageType::BindConnection;
}

#[derive(serde_derive::Serialize, serde_derive::Deserialize)]
pub struct CloseConnect {
    pub reference: u64,
}

impl TypeTagged for CloseConnect {
    const MESSAGE_TYPE: MessageType = MessageType::CloseConnect;
}
