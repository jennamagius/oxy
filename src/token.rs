/// A type that can be used to identify events from a mio::Poll. Breaks into two parts, a category and an index.
#[derive(Clone, Copy, PartialEq)]
pub struct Token(usize);

static_assertions::const_assert!(std::mem::size_of::<usize>() > 1);

#[cfg(target_endian = "big")]
const MSB_IDX: usize = 0;
#[cfg(target_endian = "little")]
const MSB_IDX: usize = std::mem::size_of::<usize>() - 1;

impl Token {
    fn msb(self) -> u8 {
        self.0.to_ne_bytes()[MSB_IDX]
    }

    pub fn category(self) -> Option<Category> {
        let msb = self.msb();
        match <Category as num_traits::FromPrimitive>::from_u8(msb) {
            Some(x) => Some(x),
            None => {
                log::debug!("Invalid category: {}", msb);
                None
            }
        }
    }

    pub fn single(self) -> Option<Single> {
        match self.category() {
            Some(Category::Single) => <Single as num_traits::FromPrimitive>::from_usize(self.idx()),
            _ => None,
        }
    }

    pub fn idx(self) -> usize {
        let mut tmp = self.0.to_ne_bytes();
        tmp[MSB_IDX] = 0;
        usize::from_ne_bytes(tmp)
    }

    pub fn new(category: Category, idx: impl TokenIdx) -> Result<Token, crate::Error> {
        let idx = match idx.wrapped() {
            TokenIdxWrapper::Raw(raw) => raw,
            TokenIdxWrapper::Single(single) => {
                match <Single as num_traits::ToPrimitive>::to_usize(&single) {
                    Some(value) => value,
                    None => {
                        log::error!("BUG: Token Single couldn't convert to a usize.");
                        return Err(crate::Error::TokenIdxTooLarge);
                    }
                }
            }
        };
        let mut result = idx.to_ne_bytes();
        if result[MSB_IDX] != 0 {
            return Err(crate::Error::TokenIdxTooLarge);
        }
        result[MSB_IDX] = match <Category as num_traits::ToPrimitive>::to_u8(&category) {
            Some(x) => x,
            None => {
                panic!("BUG: Category doesn't fit in a u8. This should compile error out due to #[repr(u8)] on Category.");
            }
        };
        Ok(Token(usize::from_ne_bytes(result)))
    }
}

impl std::convert::From<Token> for mio::Token {
    fn from(other: Token) -> mio::Token {
        mio::Token(other.0)
    }
}

impl std::convert::From<mio::Token> for Token {
    fn from(other: mio::Token) -> Token {
        Token(other.0)
    }
}

pub trait TokenIdx {
    fn wrapped(self) -> TokenIdxWrapper;
}

pub enum TokenIdxWrapper {
    Raw(usize),
    Single(Single),
}

impl TokenIdx for usize {
    fn wrapped(self) -> TokenIdxWrapper {
        TokenIdxWrapper::Raw(self)
    }
}

impl TokenIdx for Single {
    fn wrapped(self) -> TokenIdxWrapper {
        TokenIdxWrapper::Single(self)
    }
}

#[repr(u8)]
#[derive(num_derive::FromPrimitive, num_derive::ToPrimitive, PartialEq, Debug, Clone, Copy)]
pub enum Category {
    /// A category for event types that do not need an index. For events in this category the index value is the event type.
    Single,
    AcceptorBind,
    AcceptorChild,
    PortForwardBind,
    PortForwardConnection,
    InvalidCategory,
}

#[derive(num_derive::ToPrimitive, num_derive::FromPrimitive, Debug)]
pub enum Single {
    Peer,
    Signal,
    Tty,
    ServerTtyChild,
}

#[cfg(test)]
#[test]
fn token_tests() {
    for category_id in 0..=255 {
        let category = match <Category as num_traits::FromPrimitive>::from_u8(category_id) {
            None => {
                continue;
            }
            Some(x) => x,
        };
        let mut idx = [0u8; std::mem::size_of::<usize>()];
        crate::crypto_init();
        sodiumoxide::randombytes::randombytes_into(&mut idx[1..]);
        let idx = usize::from_be_bytes(idx);
        let token = Token::new(category, idx).unwrap();
        assert_eq!(token.category(), category);
        assert_eq!(token.idx(), idx);
    }
}

pub(crate) trait FindSlot {
    fn find_slot(&self) -> Option<usize>;
}

impl<T> FindSlot for std::collections::BTreeMap<usize, T> {
    fn find_slot(&self) -> Option<usize> {
        for i in 0..=usize::max_value() {
            if !self.contains_key(&i) {
                return Some(i);
            }
        }
        None
    }
}
