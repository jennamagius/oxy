use sodiumoxide::crypto::box_::curve25519xsalsa20poly1305 as box_;
use sodiumoxide::crypto::secretbox::xsalsa20poly1305 as secretbox;

#[derive(structopt::StructOpt, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct Config {
    #[structopt(long)]
    pub mode: Mode,
    #[structopt(long = "bind")]
    pub bind_addresses: Vec<std::net::SocketAddr>,
    #[structopt(long)]
    pub destination: Option<String>,
    #[structopt(long, parse(try_from_str = pubkey_decode))]
    pub server_public_key: Option<box_::PublicKey>,
    #[structopt(long, parse(try_from_str = secretkey_decode))]
    pub server_secret_key: Option<box_::SecretKey>,
    #[structopt(long, parse(try_from_str = secretkey_decode))]
    pub client_secret_key: Option<box_::SecretKey>,
    #[structopt(long, parse(try_from_str = symmetrickey_decode))]
    pub symmetric_key: Option<secretbox::Key>,
    #[structopt(long)]
    pub no_disk: bool,
    #[structopt(long)]
    pub config_dir: Option<std::path::PathBuf>,
    #[structopt(long)]
    pub reexec_path: Option<std::path::PathBuf>,
}

#[derive(Clone, Copy, Debug, serde_derive::Serialize, serde_derive::Deserialize, PartialEq)]
pub enum Mode {
    Server,
    ServeOne,
    ReverseServer,
    Client,
    ReverseClient,
    Reexec,
}

impl std::str::FromStr for Mode {
    type Err = &'static str;

    fn from_str(mode: &str) -> Result<Mode, Self::Err> {
        Ok(match mode {
            "server" | "Server" => Mode::Server,
            "serveone" | "ServeOne" => Mode::ServeOne,
            "reverseserver" | "ReverseServer" => Mode::ReverseServer,
            "client" | "Client" => Mode::Client,
            "reverseclient" | "ReverseClient" => Mode::ReverseClient,
            "reexec" | "Reexec" => Mode::Reexec,
            _ => {
                return Err("invalid mode");
            }
        })
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Side {
    Server,
    Client,
    Other,
}

impl Mode {
    pub fn side(self) -> Side {
        match self {
            Mode::Server => Side::Server,
            Mode::ServeOne => Side::Server,
            Mode::ReverseServer => Side::Server,
            Mode::Client => Side::Client,
            Mode::ReverseClient => Side::Client,
            _ => Side::Other,
        }
    }
}

fn pubkey_decode(arg: &str) -> Result<box_::PublicKey, String> {
    let decoded = match data_encoding::BASE32_NOPAD.decode(arg.as_bytes()) {
        Ok(x) => x,
        Err(err) => {
            return Err(format!("Failed to base32 decode input: {:?}", err));
        }
    };
    let key = match box_::PublicKey::from_slice(&decoded[..]) {
        Some(x) => x,
        None => {
            return Err(format!(
                "Invalid key length. Expected {}, got {}.",
                box_::PUBLICKEYBYTES,
                decoded.len()
            ));
        }
    };
    Ok(key)
}

fn secretkey_decode(arg: &str) -> Result<box_::SecretKey, String> {
    let decoded = match data_encoding::BASE32_NOPAD.decode(arg.as_bytes()) {
        Ok(x) => x,
        Err(err) => {
            return Err(format!("Failed to base32 decode input: {:?}", err));
        }
    };
    let key = match box_::SecretKey::from_slice(&decoded[..]) {
        Some(x) => x,
        None => {
            return Err(format!(
                "Invalid key length. Expected {}, got {}.",
                box_::SECRETKEYBYTES,
                decoded.len()
            ));
        }
    };
    Ok(key)
}

fn symmetrickey_decode(arg: &str) -> Result<secretbox::Key, String> {
    let decoded = match data_encoding::BASE32_NOPAD.decode(arg.as_bytes()) {
        Ok(x) => x,
        Err(err) => {
            return Err(format!("Failed to base32 decode input: {:?}", err));
        }
    };
    let key = match secretbox::Key::from_slice(&decoded[..]) {
        Some(x) => x,
        None => {
            return Err(format!(
                "Invalid key length. Expected {}, got {}.",
                secretbox::KEYBYTES,
                decoded.len()
            ));
        }
    };
    Ok(key)
}

impl Config {
    pub fn config_dir_path(&self) -> Result<std::path::PathBuf, crate::Error> {
        Ok(match self.config_dir {
            Some(ref x) => x.clone(),
            None => match dirs::home_dir() {
                Some(mut x) => {
                    x.push(".oxy");
                    x
                }
                None => {
                    log::error!("No home directory.");
                    return Err(crate::Error::NoHome);
                }
            },
        })
    }

    fn destination_dir_path(&self) -> Result<std::path::PathBuf, crate::Error> {
        let mut path = self.config_dir_path()?;
        path.push("destinations");
        let destination = match self.destination {
            Some(ref x) => x,
            None => {
                if self.mode == Mode::ReverseClient {
                    log::warn!(concat!(
                        "In reverse-client mode you must either provide --symmetric-key, ",
                        "--server-public-key, and --client-secret-key or provide ",
                        "--destination to indicate which config directory to use for ",
                        "these values."
                    ));
                }
                return Err(crate::Error::NoDestination);
            }
        };
        let destination = sanitize_destination(&destination);
        path.push(&destination);
        Ok(path)
    }

    fn server_dir_path(&self) -> Result<std::path::PathBuf, crate::Error> {
        let mut path = self.config_dir_path()?;
        path.push("server");
        Ok(path)
    }

    fn load_symmetric_key(&mut self) -> Result<(), crate::Error> {
        if self.no_disk {
            return Err(crate::Error::Disabled);
        }
        let mut path = match self.mode.side() {
            Side::Client => self.destination_dir_path()?,
            Side::Server => self.server_dir_path()?,
            Side::Other => {
                return Err(crate::Error::ProgramBug);
            }
        };
        path.push("symmetric_key");
        let mut file = std::fs::File::open(&path)?;
        let mut buf = Vec::new();
        std::io::Read::read_to_end(&mut file, &mut buf)?;
        let buf = String::from_utf8(buf)?;
        let key_data = data_encoding::BASE32_NOPAD.decode(buf.trim().as_bytes())?;
        let key = match crate::crypto::secretbox::Key::from_slice(&key_data) {
            Some(x) => x,
            None => {
                return Err(crate::Error::Crypto);
            }
        };
        log::debug!("Server symmetric key: {}", buf.trim());
        self.symmetric_key = Some(key);

        Ok(())
    }

    fn load_server_key(&mut self) -> Result<(), crate::Error> {
        if self.no_disk {
            return Err(crate::Error::Disabled);
        }
        match self.mode.side() {
            Side::Client => {
                let mut path = self.destination_dir_path()?;
                path.push("server_public_key");
                let mut file = std::fs::File::open(&path)?;
                let mut buf = Vec::new();
                std::io::Read::read_to_end(&mut file, &mut buf)?;
                let buf = String::from_utf8(buf)?;
                let key_data = data_encoding::BASE32_NOPAD.decode(buf.trim().as_bytes())?;
                let key = match crate::crypto::box_::PublicKey::from_slice(&key_data) {
                    Some(x) => x,
                    None => {
                        return Err(crate::Error::Crypto);
                    }
                };
                self.server_public_key = Some(key);
            }
            Side::Server => {
                let mut path = self.server_dir_path()?;
                path.push("secret_key");
                let mut file = std::fs::File::open(&path)?;
                let mut buf = Vec::new();
                std::io::Read::read_to_end(&mut file, &mut buf)?;
                let buf = String::from_utf8(buf)?;
                let key_data = data_encoding::BASE32_NOPAD.decode(buf.trim().as_bytes())?;
                let key = match crate::crypto::box_::SecretKey::from_slice(&key_data) {
                    Some(x) => x,
                    None => {
                        return Err(crate::Error::Crypto);
                    }
                };
                let public_key = key.public_key();
                log::debug!(
                    "Server public key: {}",
                    data_encoding::BASE32_NOPAD.encode(&public_key[..])
                );
                self.server_public_key = Some(public_key);
                self.server_secret_key = Some(key);
            }
            Side::Other => {
                return Err(crate::Error::ProgramBug);
            }
        };
        Ok(())
    }

    fn load_or_generate_symmetric_key(&mut self) -> Result<(), crate::Error> {
        match self.load_symmetric_key() {
            Ok(()) => {
                log::trace!("Successfully loaded symmetric key.");
                return Ok(());
            }
            Err(err) => {
                log::trace!("Error loading symmetric key: {:?}", err);
                // fall through to the rest of the function
            }
        }

        match self.mode.side() {
            Side::Client => {
                log::warn!("No symmetric key loaded.");
                return Err(crate::Error::MissingConfigOption);
            }
            Side::Other => {
                return Err(crate::Error::ProgramBug);
            }
            Side::Server => (),
        }
        let key = crate::crypto::secretbox::gen_key();
        log::info!(
            "No symmetric key provided. Using {}.",
            data_encoding::BASE32_NOPAD.encode(&key.0[..])
        );

        self.symmetric_key = Some(key.clone());

        if self.no_disk {
            return Ok(());
        }
        let server_dir_path = self.server_dir_path()?;
        mkdir(&server_dir_path)?;
        let mut symmetric_key_path = server_dir_path.clone();
        symmetric_key_path.push("symmetric_key");
        let mut open_options = std::fs::OpenOptions::new();
        open_options.write(true);
        open_options.create(true);
        open_options.create_new(true);
        let mut file = open_options.open(&symmetric_key_path)?;
        let mut data = data_encoding::BASE32_NOPAD
            .encode(&key.0[..])
            .as_bytes()
            .to_vec();
        data.push(b'\n');
        std::io::Write::write_all(&mut file, &data[..])?;
        log::info!("Symmetric key saved at {:?}", symmetric_key_path);
        Ok(())
    }

    fn load_or_generate_server_key(&mut self) -> Result<(), crate::Error> {
        match self.load_server_key() {
            Ok(()) => {
                return Ok(());
            }
            Err(err) => {
                log::trace!("Error loading server key: {:?}", err);
            }
        }

        log::trace!("No server key loaded.");

        if self.mode.side() == Side::Client {
            return Err(crate::Error::MissingConfigOption);
        }

        let (pk, sk) = crate::crypto::box_::gen_keypair();
        log::info!(
            "No host key provided. Using pk: {} sk: {}.",
            data_encoding::BASE32_NOPAD.encode(&pk.0),
            data_encoding::BASE32_NOPAD.encode(&sk.0),
        );

        self.server_public_key = Some(pk);
        self.server_secret_key = Some(sk.clone());

        if self.no_disk {
            return Ok(());
        }

        let base_path = self.server_dir_path()?;
        mkdir(&base_path)?;
        let mut public_key_path = base_path.clone();
        public_key_path.push("public_key");
        let mut secret_key_path = base_path;
        secret_key_path.push("secret_key");
        let mut public_key_file = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .create_new(true)
            .open(&public_key_path)?;
        let mut secret_key_file = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .create_new(true)
            .open(&secret_key_path)?;
        let public_key_data: Vec<u8> = data_encoding::BASE32_NOPAD
            .encode(&pk.0[..])
            .as_bytes()
            .iter()
            .copied()
            .chain(b"\n".iter().copied())
            .collect();
        let secret_key_data: Vec<u8> = data_encoding::BASE32_NOPAD
            .encode(&sk.0[..])
            .as_bytes()
            .iter()
            .copied()
            .chain(b"\n".iter().copied())
            .collect();
        std::io::Write::write_all(&mut public_key_file, &public_key_data[..])?;
        std::io::Write::write_all(&mut secret_key_file, &secret_key_data[..])?;
        Ok(())
    }

    fn load_client_key(&mut self) -> Result<(), crate::Error> {
        if self.no_disk {
            return Err(crate::Error::Disabled);
        }

        let mut path = self.destination_dir_path()?;
        path.push("client_secret_key");
        let mut file = std::fs::File::open(&path)?;
        let mut buf = Vec::new();
        std::io::Read::read_to_end(&mut file, &mut buf)?;
        let buf = String::from_utf8(buf)?;
        let key_data = data_encoding::BASE32_NOPAD.decode(buf.trim().as_bytes())?;
        let key = match crate::crypto::box_::SecretKey::from_slice(&key_data) {
            Some(x) => x,
            None => {
                return Err(crate::Error::Crypto);
            }
        };
        if log::log_enabled!(log::Level::Trace) {
            let public_key = key.public_key();
            log::trace!(
                "Loaded client secret key. Client public key: {}",
                data_encoding::BASE32_NOPAD.encode(&public_key[..])
            );
        }
        self.client_secret_key = Some(key);
        Ok(())
    }

    fn load_or_generate_client_key(&mut self) -> Result<(), crate::Error> {
        match self.load_client_key() {
            Ok(()) => {
                return Ok(());
            }
            Err(err) => {
                log::trace!("Failed to load client key: {:?}", err);
            }
        }

        let (pk, sk) = crate::crypto::box_::gen_keypair();
        log::info!(
            "No client key provided. Generated public key: {}",
            data_encoding::BASE32_NOPAD.encode(&pk.0),
        );

        self.client_secret_key = Some(sk.clone());

        if self.no_disk {
            return Ok(());
        }

        let base_path = self.destination_dir_path()?;
        mkdir(&base_path)?;
        let mut public_key_path = base_path.clone();
        public_key_path.push("client_public_key");
        let mut secret_key_path = base_path;
        secret_key_path.push("client_secret_key");
        let mut public_key_file = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .create_new(true)
            .open(&public_key_path)?;
        let mut secret_key_file = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .create_new(true)
            .open(&secret_key_path)?;
        let public_key_data: Vec<u8> = data_encoding::BASE32_NOPAD
            .encode(&pk.0[..])
            .as_bytes()
            .iter()
            .copied()
            .chain(b"\n".iter().copied())
            .collect();
        let secret_key_data: Vec<u8> = data_encoding::BASE32_NOPAD
            .encode(&sk.0[..])
            .as_bytes()
            .iter()
            .copied()
            .chain(b"\n".iter().copied())
            .collect();
        std::io::Write::write_all(&mut public_key_file, &public_key_data[..])?;
        std::io::Write::write_all(&mut secret_key_file, &secret_key_data[..])?;
        Ok(())
    }

    pub(crate) fn prepare_secrets(&mut self) -> Result<(), crate::Error> {
        crate::crypto_init()?;

        if self.mode.side() == Side::Client && self.client_secret_key.is_none() {
            self.load_or_generate_client_key()?;
        }

        if (self.mode.side() == Side::Client && self.server_public_key.is_none())
            || (self.mode.side() == Side::Server && self.server_secret_key.is_none())
        {
            self.load_or_generate_server_key()?;
        }

        if self.symmetric_key.is_none() {
            self.load_or_generate_symmetric_key()?;
        }

        Ok(())
    }
}

fn mkdir(path: impl std::convert::AsRef<std::path::Path>) -> Result<(), std::io::Error> {
    let mut dir_builder = std::fs::DirBuilder::new();
    std::os::unix::fs::DirBuilderExt::mode(&mut dir_builder, 0o700);
    dir_builder.recursive(true);
    dir_builder.create(path)
}

fn valid_sanitized_destination_char(x: char) -> bool {
    // We ban : to steer clear of Alternate Data Streams syntax.
    !x.is_whitespace() && !x.is_control() && !"/\\:".chars().any(|y| y == x)
}

fn sanitize_destination(destination: &str) -> String {
    destination
        .chars()
        .map(|x| x.to_ascii_lowercase())
        .map(|x| {
            if valid_sanitized_destination_char(x) {
                x
            } else {
                '_'
            }
        })
        .collect()
}
