pub use sodiumoxide::crypto::box_::curve25519xsalsa20poly1305 as box_;
pub use sodiumoxide::crypto::sealedbox::curve25519blake2bxsalsa20poly1305 as sealedbox;
pub use sodiumoxide::crypto::secretbox::xsalsa20poly1305 as secretbox;
pub use sodiumoxide::crypto::secretstream::xchacha20poly1305 as secretstream;
pub use sodiumoxide::crypto::stream::xsalsa20 as stream;
