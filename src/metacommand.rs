#[derive(structopt::StructOpt, Debug)]
#[structopt(setting(structopt::clap::AppSettings::NoBinaryName))]
pub enum Metacommand {
    /// Exit the Oxy client.
    #[structopt(alias = "quit")]
    Exit,
    /// Bind bind_spec on the server side and forward connections to connect_destination.
    #[structopt(alias = "R")]
    RemotePortForward {
        bind_spec: std::net::SocketAddr,
        connect_destination: String,
    },
    /// Bind bind_spec on the client side and forward connections to connect_destination.
    #[structopt(alias = "L")]
    LocalPortForward {
        bind_spec: std::net::SocketAddr,
        connect_destination: String,
    },
}
