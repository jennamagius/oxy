struct Acceptor {
    poll: mio::Poll,
    binds: Vec<mio::net::TcpListener>,
    config: crate::config::Config,
    signals: Option<signal_hook::iterator::Signals>,
    pending_children: std::collections::BTreeMap<usize, PendingChild>,
}

struct PendingChild {
    peer_socket: Option<std::net::TcpStream>,
    unix_fd: std::os::unix::io::RawFd,
    write_buffer: Vec<u8>,
    pid: u32,
}

impl Acceptor {
    fn new(config: crate::config::Config) -> Result<Acceptor, crate::Error> {
        let poll = match mio::Poll::new() {
            Ok(poll) => poll,
            Err(err) => {
                return Err(crate::Error::Mio(err));
            }
        };
        let binds = make_binds(&config, &poll)?;
        let signals = match config.mode {
            crate::config::Mode::Server => {
                let signals = signal_hook::iterator::Signals::new(&[signal_hook::SIGCHLD])?;
                let token = crate::token::Token::new(
                    crate::token::Category::Single,
                    crate::token::Single::Signal,
                )?;
                poll.register(
                    &signals,
                    token.into(),
                    mio::Ready::readable(),
                    mio::PollOpt::edge(),
                )
                .map_err(|x| crate::Error::Mio(x))?;
                Some(signals)
            }
            _ => None,
        };
        let pending_children = std::collections::BTreeMap::default();
        Ok(Acceptor {
            poll,
            binds,
            config,
            signals,
            pending_children,
        })
    }

    fn accept_one(&mut self) -> Result<std::net::TcpStream, crate::Error> {
        let mut events = mio::Events::with_capacity(1);
        loop {
            match self.poll.poll(&mut events, None) {
                Ok(_amt) => (),
                Err(err) => {
                    log::warn!("Unexpected poll error: {}.", err);
                }
            }
            for event in &events {
                match self.handle_event(event) {
                    Ok(Some(x)) => {
                        return Ok(x);
                    }
                    Ok(None) => (),
                    Err(err) => {
                        return Err(err);
                    }
                }
            }
        }
    }

    fn create_child(&mut self, peer_socket: std::net::TcpStream) -> Result<(), crate::Error> {
        let mut child_idx = 0;
        loop {
            if !self.pending_children.contains_key(&child_idx) {
                break;
            }
            child_idx = match child_idx.checked_add(1) {
                Some(x) => x,
                None => {
                    return Err(crate::Error::ResourceLimit);
                }
            }
        }
        let token = match crate::token::Token::new(crate::token::Category::AcceptorChild, child_idx)
        {
            Ok(x) => x,
            Err(_) => {
                return Err(crate::Error::ResourceLimit);
            }
        };

        let reexec_path = match self.config.reexec_path {
            Some(ref x) => x,
            None => {
                return Err(crate::Error::ProgramBug);
            }
        };

        let serialized_config = serde_cbor::to_vec(&self.config)?;
        let serialized_len =
            match <u32 as std::convert::TryFrom<_>>::try_from(serialized_config.len()) {
                Ok(x) => x,
                Err(_) => {
                    log::error!("Configuration too large");
                    return Err(crate::Error::Other);
                }
            };
        let mut write_buffer = serialized_len.to_be_bytes().to_vec();
        write_buffer.extend(&serialized_config);

        let (parent_socket, child_socket) = nix::sys::socket::socketpair(
            nix::sys::socket::AddressFamily::Unix,
            nix::sys::socket::SockType::Stream,
            None,
            nix::sys::socket::SockFlag::SOCK_NONBLOCK | nix::sys::socket::SockFlag::SOCK_CLOEXEC,
        )?;

        self.poll.register(
            &mio::unix::EventedFd(&parent_socket),
            mio::Token::from(token),
            mio::Ready::writable(),
            mio::PollOpt::edge(),
        )?;

        let mut command = std::process::Command::new(reexec_path);
        command.arg("--mode=reexec");
        unsafe {
            std::os::unix::process::CommandExt::pre_exec(&mut command, move || {
                // The purpose of this dup is less to put child_socket on STDIN_FILENO and more
                // to clear the O_CLOEXEC flag. We want to clear O_CLOEXEC after the fork to avoid
                // leaking the FD on a race condition.
                match nix::unistd::dup2(child_socket, libc::STDIN_FILENO) {
                    Ok(_) => Ok(()),
                    Err(_) => Err(std::io::Error::last_os_error()),
                }
            });
        }
        let process = command.spawn()?;
        nix::unistd::close(child_socket)?;
        let pid = process.id();
        log::trace!("Spawned child at pid {}.", pid);

        let pending_child = PendingChild {
            unix_fd: parent_socket,
            peer_socket: Some(peer_socket),
            write_buffer,
            pid,
        };
        self.pending_children.insert(child_idx, pending_child);
        Ok(())
    }

    fn accept_forever(&mut self) -> Result<!, crate::Error> {
        loop {
            let socket = self.accept_one()?;
            self.create_child(socket)?;
        }
    }

    fn bind_event(&mut self, idx: usize) -> Result<std::net::TcpStream, crate::Error> {
        let bind = match self.binds.get_mut(idx) {
            Some(x) => x,
            None => {
                return Err(crate::Error::InvalidEvent);
            }
        };
        Ok(bind.accept_std()?.0)
    }

    fn handle_signal(&mut self) -> Result<(), crate::Error> {
        let signals = match self.signals {
            Some(ref mut x) => x,
            None => {
                return Err(crate::Error::InvalidEvent);
            }
        };
        for signal in signals.pending() {
            loop {
                match nix::sys::wait::waitpid(None, Some(nix::sys::wait::WaitPidFlag::WNOHANG)) {
                    Ok(result) => {
                        log::trace!("waitpid result: {:?}", result);
                    }
                    _ => {
                        break;
                    }
                }
            }
            log::trace!("Received signal: {}.", signal);
        }
        Ok(())
    }

    fn child_event(&mut self, child_idx: usize) -> Result<(), crate::Error> {
        let pending_child = match self.pending_children.get_mut(&child_idx) {
            Some(x) => x,
            None => {
                return Err(crate::Error::InvalidEvent);
            }
        };
        let mut write_idx = 0;
        loop {
            if write_idx > pending_child.write_buffer.len() {
                log::warn!("Child write_idx > child write_buffer. This shouldn't happen.");
                return Err(crate::Error::ProgramBug);
            }
            if write_idx == pending_child.write_buffer.len() {
                pending_child.write_buffer.clear();
                break;
            }
            let written_amt = match nix::unistd::write(
                pending_child.unix_fd,
                &pending_child.write_buffer[write_idx..],
            ) {
                Ok(x) => x,
                Err(err) => {
                    pending_child.write_buffer.drain(..write_idx);
                    match err {
                        nix::Error::Sys(nix::errno::EWOULDBLOCK) => {
                            return Ok(());
                        }
                        _ => {
                            log::warn!("Error while writing to child: {}.", err);
                            return Err(crate::Error::Nix(err));
                        }
                    }
                }
            };
            write_idx = match write_idx.checked_add(written_amt) {
                Some(x) => x,
                None => {
                    return Err(crate::Error::Overflow);
                }
            }
        }
        let peer_socket = match pending_child.peer_socket {
            Some(ref x) => x,
            None => {
                return Err(crate::Error::ProgramBug);
            }
        };
        let peer_address = peer_socket.peer_addr();
        let peer_fd = std::os::unix::io::AsRawFd::as_raw_fd(peer_socket);
        let fd_list = [peer_fd];

        // The reason we ScmRights the peer connection over instead of just passing it through the
        // fork is: on Windows we're obliged to use WsaDuplicateSocket and that's going to flow
        // very similarly to the ScmRights flow, so this helps keep things similar across platforms
        let control_message = nix::sys::socket::ControlMessage::ScmRights(&fd_list[..]);

        let ignore = [0u8; 1];
        let ignore_iovec = nix::sys::uio::IoVec::from_slice(&ignore[..]);

        nix::sys::socket::sendmsg(
            pending_child.unix_fd,
            &[ignore_iovec],
            &[control_message],
            nix::sys::socket::MsgFlags::MSG_DONTWAIT,
            None,
        )?;
        pending_child.peer_socket.take();
        self.poll
            .deregister(&mio::unix::EventedFd(&pending_child.unix_fd))
            .map_err(|x| crate::Error::Mio(x))?;
        nix::unistd::close(pending_child.unix_fd)?;
        log::trace!(
            "Finished transferring data to child pid {} (peer address: {:?}).",
            pending_child.pid,
            peer_address,
        );
        self.pending_children.remove(&child_idx);
        Ok(())
    }

    fn handle_event(
        &mut self,
        event: mio::Event,
    ) -> Result<Option<std::net::TcpStream>, crate::Error> {
        let token = crate::token::Token::from(event.token());
        match token.category() {
            Some(crate::token::Category::AcceptorBind) => match self.bind_event(token.idx()) {
                Ok(socket) => Ok(Some(socket)),
                Err(_) => Ok(None),
            },
            Some(crate::token::Category::AcceptorChild) => {
                self.child_event(token.idx())?;
                Ok(None)
            }
            Some(crate::token::Category::Single) => match token.single() {
                Some(crate::token::Single::Signal) => {
                    self.handle_signal()?;
                    Ok(None)
                }
                _ => {
                    log::warn!("Unknown single event in acceptor.");
                    Ok(None)
                }
            },
            _ => {
                log::warn!("Unknown event category in acceptor.");
                Ok(None)
            }
        }
    }
}

fn make_binds(
    config: &crate::config::Config,
    poll: &mio::Poll,
) -> Result<Vec<mio::net::TcpListener>, crate::Error> {
    let mut binds = Vec::new();
    for (idx, bind_address) in config.bind_addresses.iter().enumerate() {
        let bind = match mio::net::TcpListener::bind(bind_address) {
            Ok(x) => x,
            Err(err) => {
                log::warn!("Failed to bind {}: {}.", bind_address, err);
                continue;
            }
        };
        let token = match crate::token::Token::new(crate::token::Category::AcceptorBind, idx) {
            Ok(x) => x,
            Err(_err) => {
                log::warn!(
                    "Failed to make bind token for {} at idx {}.",
                    bind_address,
                    idx
                );
                continue;
            }
        };
        match poll.register(
            &bind,
            token.into(),
            mio::Ready::readable(),
            mio::PollOpt::edge(),
        ) {
            Ok(_) => (),
            Err(err) => {
                log::warn!("Failed to register bind {}: {}.", bind_address, err);
                continue;
            }
        }
        log::trace!("Successfully bound {}.", bind_address);
        binds.push(bind);
    }
    Ok(binds)
}

pub(crate) fn accept_one(
    mut config: crate::config::Config,
) -> Result<crate::oxy::Oxy, crate::Error> {
    config.prepare_secrets()?;
    let mut acceptor = Acceptor::new(config)?;
    let socket = acceptor.accept_one()?;
    let socket = match mio::net::TcpStream::from_stream(socket) {
        Ok(socket) => socket,
        Err(err) => {
            log::error!("Failed to mio-ize socket: {}", err);
            return Err(crate::Error::Mio(err));
        }
    };
    crate::oxy::Oxy::new(acceptor.config, socket)
}

pub(crate) fn accept_forever(mut config: crate::config::Config) -> Result<!, crate::Error> {
    config.prepare_secrets()?;
    if config.reexec_path.is_none() {
        log::error!("No reexec path provided");
        return Err(crate::Error::MissingConfigOption);
    }
    if config.reexec_path.as_ref().map(|x| x.is_absolute()) != Some(true) {
        log::error!("reexec-path is not an absolute path.");
        return Err(crate::Error::Other);
    }
    if nix::unistd::getuid() != nix::unistd::geteuid() {
        log::error!("Don't run oxy setuid.");
        return Err(crate::Error::Other);
    }
    let mut acceptor = Acceptor::new(config)?;
    acceptor.accept_forever()
}
