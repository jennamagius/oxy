const BUFFER_CAPACITY: usize = 1024 * 1024;

pub struct Oxy {
    poll: mio::Poll,
    peer: mio::net::TcpStream,
    config: crate::config::Config,
    finished: bool,
    read_buffer: Vec<u8>,
    write_buffer: Vec<u8>,
    message_construction_buffer: Vec<u8>,
    signals: signal_hook::iterator::Signals,
    send_stream: Option<crate::crypto::secretstream::Stream<crate::crypto::secretstream::Push>>,
    recv_stream: Option<crate::crypto::secretstream::Stream<crate::crypto::secretstream::Pull>>,
    send_length_key: Option<crate::crypto::stream::Key>,
    send_length_nonce: Option<crate::crypto::stream::Nonce>,
    recv_length_key: Option<crate::crypto::stream::Key>,
    recv_length_nonce: Option<crate::crypto::stream::Nonce>,
    recv_message_ticker: u64,
    send_message_ticker: u64,
    port_forward_binds: std::collections::BTreeMap<usize, PortForwardBind>,
    port_forward_connections: std::collections::BTreeMap<usize, PortForwardConnection>,
    message_id_reference: std::collections::BTreeMap<MessageId, MessageReference>,
    side_data: SideData,
}

enum SideData {
    Client(Box<ClientData>),
    Server(Box<ServerData>),
}

#[derive(Clone, Copy, PartialEq, PartialOrd, Eq, Ord)]
enum MessageDirection {
    Sent,
    Recieved,
}

enum MessageReference {
    RemoteBind(RemoteBindReference),
}

struct RemoteBindReference {
    connect_destination: String,
}

#[derive(Clone, Copy, PartialEq, PartialOrd, Eq, Ord)]
struct MessageId(pub u64, pub MessageDirection);

#[derive(Default)]
struct ServerData {
    pty_fd: Option<std::os::unix::io::RawFd>,
    pty_pid: Option<nix::unistd::Pid>,
}

#[derive(Default)]
struct ClientData {
    raw_mode_holder: Option<termion::raw::RawTerminal<std::fs::File>>,
    serversend_stream_key: Option<crate::crypto::secretstream::Key>,
    terminfo: Option<terminfo::Database>,
    exit_sequence: Vec<u8>,
    escape_sequence: Vec<u8>,
    linefeed: Option<linefeed::interface::Interface<linefeed::terminal::DefaultTerminal>>,
    input_mode: InputMode,
}

struct PortForwardConnection {
    reference_id: u64,
    connection: mio::net::TcpStream,
    orientation: ConnectionOrientation,
}

#[derive(PartialEq, Debug, Clone, Copy)]
enum ConnectionOrientation {
    ServerAccept,
    ServerConnect,
}

struct PortForwardBind {
    reference_id: Option<u64>,
    connect_destination: Option<String>,
    bind: mio::net::TcpListener,
}

enum InputMode {
    Raw,
    Linefeed,
}

impl Default for InputMode {
    fn default() -> InputMode {
        InputMode::Raw
    }
}

enum HandshakeStatus {
    Incomplete,
    Complete,
}

enum ConsumeMessageStatus {
    Finished,
    NotFinished,
}

impl SideData {
    fn client_data(&self) -> Result<&ClientData, crate::Error> {
        match self {
            SideData::Client(ref x) => Ok(x),
            _ => Err(crate::Error::IncorrectMode),
        }
    }

    fn client_data_mut(&mut self) -> Result<&mut ClientData, crate::Error> {
        match self {
            SideData::Client(ref mut x) => Ok(x),
            _ => Err(crate::Error::IncorrectMode),
        }
    }

    fn server_data(&self) -> Result<&ServerData, crate::Error> {
        match self {
            SideData::Server(ref x) => Ok(x),
            _ => Err(crate::Error::IncorrectMode),
        }
    }

    fn server_data_mut(&mut self) -> Result<&mut ServerData, crate::Error> {
        match self {
            SideData::Server(ref mut x) => Ok(x),
            _ => Err(crate::Error::IncorrectMode),
        }
    }
}

impl Oxy {
    pub fn new(
        config: crate::config::Config,
        peer: mio::net::TcpStream,
    ) -> Result<Oxy, crate::Error> {
        log::trace!("Creating oxy instance.");
        let poll = match mio::Poll::new() {
            Ok(poll) => poll,
            Err(err) => {
                log::error!("Failed to create poll. {}", err);
                return Err(crate::Error::Mio(err));
            }
        };
        let finished = false;
        let read_buffer = Vec::with_capacity(BUFFER_CAPACITY);
        let write_buffer = Vec::with_capacity(BUFFER_CAPACITY);
        let signals = signal_hook::iterator::Signals::new(&[])?;
        let side_data = match config.mode.side() {
            crate::config::Side::Client => SideData::Client(Box::new(ClientData::default())),
            crate::config::Side::Server => SideData::Server(Box::new(ServerData::default())),
            _ => {
                return Err(crate::Error::ProgramBug);
            }
        };
        let mut result = Oxy {
            poll,
            peer,
            config,
            finished,
            read_buffer,
            write_buffer,
            signals,
            send_stream: None,
            recv_stream: None,
            send_length_key: None,
            send_length_nonce: None,
            recv_length_key: None,
            recv_length_nonce: None,
            message_construction_buffer: Vec::new(),
            recv_message_ticker: 0,
            send_message_ticker: 0,
            side_data,
            port_forward_binds: std::collections::BTreeMap::new(),
            port_forward_connections: std::collections::BTreeMap::new(),
            message_id_reference: std::collections::BTreeMap::new(),
        };
        result.init()?;
        Ok(result)
    }

    fn init(&mut self) -> Result<(), crate::Error> {
        log::trace!("Initializing oxy instance.");
        self.register_peer()?;
        self.register_signals()?;
        if self.config.mode.side() == crate::config::Side::Client {
            self.load_terminfo()?;
            self.init_linefeed()?;
        }
        Ok(())
    }

    fn init_linefeed(&mut self) -> Result<(), crate::Error> {
        match linefeed::interface::Interface::new("oxy") {
            Ok(x) => {
                let _ = x.set_prompt("oxy> ");
                self.side_data.client_data_mut()?.linefeed = Some(x);
            }
            Err(err) => {
                log::warn!("Failed to init linefeed: {}.", err);
            }
        }
        Ok(())
    }

    fn load_terminfo(&mut self) -> Result<(), crate::Error> {
        let client_data = self.side_data.client_data_mut()?;
        client_data.exit_sequence = [27, 91, 50, 52, 126].to_vec();
        client_data.escape_sequence = [27, 91, 50, 49, 126].to_vec();

        let db = match terminfo::Database::from_env() {
            Ok(x) => x,
            Err(err) => {
                log::warn!("Failed to load terminfo: {}.", err);
                return Ok(());
            }
        };

        match db.get::<terminfo::capability::KeyF12>() {
            Some(x) => match x.expand().to_vec() {
                Ok(x) => {
                    log::trace!("Successfully got {:?} as exit_sequence from terminfo.", x);
                    client_data.exit_sequence = x;
                }
                Err(_) => (),
            },
            None => (),
        }

        match db.get::<terminfo::capability::KeyF10>() {
            Some(x) => match x.expand().to_vec() {
                Ok(x) => {
                    log::trace!("Successfully got {:?} as escape_sequence from terminfo.", x);
                    client_data.escape_sequence = x;
                }
                Err(_) => (),
            },
            None => (),
        }

        client_data.terminfo = Some(db);
        Ok(())
    }

    fn register_signals(&mut self) -> Result<(), crate::Error> {
        log::trace!("Registering signal handlers.");
        match self.config.mode.side() {
            crate::config::Side::Client => {
                self.signals.add_signal(signal_hook::SIGWINCH)?;
            }
            crate::config::Side::Server => {
                self.signals.add_signal(signal_hook::SIGCHLD)?;
            }
            _ => (),
        }
        let token =
            crate::token::Token::new(crate::token::Category::Single, crate::token::Single::Signal)?;
        self.poll
            .register(
                &self.signals,
                mio::Token::from(token),
                mio::Ready::readable(),
                mio::PollOpt::edge(),
            )
            .map_err(|x| crate::Error::Mio(x))?;
        Ok(())
    }

    fn register_peer(&mut self) -> Result<(), crate::Error> {
        let mut interest = mio::Ready::readable();
        if self.config.mode.side() == crate::config::Side::Client {
            interest.insert(mio::Ready::writable());
        }
        let token =
            crate::token::Token::new(crate::token::Category::Single, crate::token::Single::Peer)?;
        match self
            .poll
            .register(&self.peer, token.into(), interest, mio::PollOpt::edge())
        {
            Ok(_) => (),
            Err(err) => {
                log::error!("Failed to register peer: {}", err);
                return Err(crate::Error::Mio(err));
            }
        }
        Ok(())
    }

    fn send_client_hello(&mut self) -> Result<(), crate::Error> {
        crate::crypto_init()?;

        let clientsend_length_key = crate::crypto::stream::gen_key();
        self.send_length_key = Some(clientsend_length_key.clone());
        let clientsend_length_key = clientsend_length_key.0;
        let clientsend_length_nonce = crate::crypto::stream::gen_nonce();
        self.send_length_nonce = Some(clientsend_length_nonce);
        let clientsend_length_nonce = clientsend_length_nonce.0;
        let serversend_length_key = crate::crypto::stream::gen_key();
        self.recv_length_key = Some(serversend_length_key.clone());
        let serversend_length_key = serversend_length_key.0;
        let serversend_length_nonce = crate::crypto::stream::gen_nonce();
        self.recv_length_nonce = Some(serversend_length_nonce);
        let serversend_length_nonce = serversend_length_nonce.0;
        let clientsend_stream_key = crate::crypto::secretstream::gen_key();
        let (clientsend_stream, clientsend_stream_header) =
            match crate::crypto::secretstream::Stream::init_push(&clientsend_stream_key) {
                Ok(x) => x,
                Err(_) => {
                    log::error!("Unexpected error initializing secretstream");
                    return Err(crate::Error::Crypto);
                }
            };
        self.send_stream = Some(clientsend_stream);
        let clientsend_stream_key = clientsend_stream_key.0;
        let clientsend_stream_header = clientsend_stream_header.0;
        let serversend_stream_key = crate::crypto::secretstream::gen_key();
        self.side_data.client_data_mut()?.serversend_stream_key =
            Some(serversend_stream_key.clone());
        let serversend_stream_key = serversend_stream_key.0;
        let inner3 = crate::message::ClientHelloInner3 {
            clientsend_length_key,
            clientsend_length_nonce,
            serversend_length_key,
            serversend_length_nonce,
            clientsend_stream_key,
            clientsend_stream_header,
            serversend_stream_key,
        };
        let mut inner3_bytes =
            [0u8; <crate::message::ClientHelloInner3 as bytepack::BytePack>::WIDTH];
        bytepack::BytePack::to_slice(&inner3, &mut inner3_bytes[..]);

        let client_secretkey = match self.config.client_secret_key {
            Some(ref x) => x,
            None => {
                log::error!("No client secret key provided.");
                return Err(crate::Error::MissingConfigOption);
            }
        };
        let client_pubkey = client_secretkey.public_key();
        let server_pubkey = match self.config.server_public_key {
            Some(ref x) => x,
            None => {
                log::error!("No server public key provided.");
                return Err(crate::Error::MissingConfigOption);
            }
        };
        let asymmetric2_nonce = crate::crypto::box_::gen_nonce();
        let asymmetric2_mac = crate::crypto::box_::seal_detached(
            &mut inner3_bytes,
            &asymmetric2_nonce,
            &server_pubkey,
            client_secretkey,
        )
        .0;
        let inner2 = crate::message::ClientHelloInner2 {
            asymmetric2_ciphertext: inner3_bytes,
            asymmetric2_mac,
            asymmetric2_nonce: asymmetric2_nonce.0,
            client_pubkey: client_pubkey.0,
        };
        let mut inner2_bytes =
            [0u8; <crate::message::ClientHelloInner2 as bytepack::BytePack>::WIDTH];
        bytepack::BytePack::to_slice(&inner2, &mut inner2_bytes[..]);

        let sealedbox = crate::crypto::sealedbox::seal(&inner2_bytes, server_pubkey); // I would prefer a seal_detached, but there isn't one
        let mut inner1_bytes =
            [0u8; <crate::message::ClientHelloInner1 as bytepack::BytePack>::WIDTH];
        if sealedbox.len() != inner1_bytes.len() {
            log::warn!("Sealedbox went wrong.");
            return Err(crate::Error::Crypto);
        }
        inner1_bytes.copy_from_slice(&sealedbox);

        let symmetric_nonce = crate::crypto::secretbox::gen_nonce();
        let symmetric_key = match self.config.symmetric_key {
            Some(ref x) => x,
            None => {
                log::error!("No symmetric key provided.");
                return Err(crate::Error::MissingConfigOption);
            }
        };
        let symmetric_mac = crate::crypto::secretbox::seal_detached(
            &mut inner1_bytes,
            &symmetric_nonce,
            &symmetric_key,
        )
        .0;
        let client_hello = crate::message::ClientHello {
            symmetric_nonce: symmetric_nonce.0,
            symmetric_mac,
            symmetric_ciphertext: inner1_bytes,
        };
        let mut client_hello_bytes =
            [0u8; <crate::message::ClientHello as bytepack::BytePack>::WIDTH];
        bytepack::BytePack::to_slice(&client_hello, &mut client_hello_bytes[..]);

        self.write_buffer.extend(&client_hello_bytes[..]);

        Ok(())
    }

    pub fn run(&mut self) -> Result<(), crate::Error> {
        if self.config.mode.side() == crate::config::Side::Client {
            self.send_client_hello()?;
        }
        self.run_handshake()?;
        self.run_post_handshake()?;
        Ok(())
    }

    fn run_handshake(&mut self) -> Result<(), crate::Error> {
        let mut events = mio::Events::with_capacity(1);
        while !self.finished {
            match self.poll.poll(&mut events, None) {
                Ok(_) => (),
                Err(err) => {
                    log::debug!("Poll error: {}", err);
                }
            }
            for event in &events {
                match self.handle_handshake_event(event) {
                    Ok(HandshakeStatus::Complete) => {
                        return Ok(());
                    }
                    Ok(HandshakeStatus::Incomplete) => (),
                    Err(err) => {
                        return Err(err);
                    }
                }
            }
        }
        Ok(())
    }

    fn run_post_handshake(&mut self) -> Result<(), crate::Error> {
        log::trace!("Starting post-handshake run loop.");
        let mut events = mio::Events::with_capacity(32);
        while !self.finished {
            match self.poll.poll(&mut events, None) {
                Ok(_) => (),
                Err(err) => {
                    log::debug!("Poll error: {}", err);
                }
            }
            for event in &events {
                self.handle_event(event)?;
            }
            self.reregister_peer()?;
        }
        Ok(())
    }

    pub fn peer_interest(&self) -> mio::Ready {
        let mut interest = mio::Ready::empty();

        if self.read_buffer.len() < BUFFER_CAPACITY {
            interest.insert(mio::Ready::readable());
        }
        if !self.write_buffer.is_empty() {
            interest.insert(mio::Ready::writable());
        }

        interest
    }

    pub fn reregister_peer(&mut self) -> Result<(), crate::Error> {
        let interest = self.peer_interest();
        let token =
            crate::token::Token::new(crate::token::Category::Single, crate::token::Single::Peer)?;
        match self
            .poll
            .reregister(&self.peer, token.into(), interest, mio::PollOpt::edge())
        {
            Ok(_) => (),
            Err(err) => {
                return Err(crate::Error::Mio(err));
            }
        }
        Ok(())
    }

    pub fn peer_read(&mut self) -> Result<(), crate::Error> {
        loop {
            if self.read_buffer.len() >= BUFFER_CAPACITY {
                return Ok(());
            }
            let valid_len = self.read_buffer.len();
            self.read_buffer.resize(BUFFER_CAPACITY, 0);
            match std::io::Read::read(&mut self.peer, &mut self.read_buffer[valid_len..]) {
                Ok(0) => {
                    log::info!("Connection lost.");
                    match self.side_data.server_data() {
                        Ok(server_data) => match server_data.pty_pid {
                            Some(pid) => {
                                log::trace!("Sending SIGHUP to {}.", pid);
                                let _ =
                                    nix::sys::signal::kill(pid, nix::sys::signal::Signal::SIGHUP);
                            }
                            _ => (),
                        },
                        _ => (),
                    }
                    self.finished = true;
                    self.read_buffer.truncate(valid_len);
                    return Ok(());
                }
                Ok(amt) => {
                    log::trace!("Read {}.", amt);
                    let new_valid_len = match valid_len.checked_add(amt) {
                        Some(x) => x,
                        None => {
                            const MSG: &str =
                                "BUG: read more data than fits in a usize. That shouldn't be possible.";
                            log::error!("{}", MSG);
                            panic!(MSG);
                        }
                    };
                    self.read_buffer.truncate(new_valid_len);
                }
                Err(err) => {
                    log::trace!("Read error: {}.", err);
                    self.read_buffer.truncate(valid_len);
                    match err.kind() {
                        std::io::ErrorKind::WouldBlock => {
                            return Ok(());
                        }
                        std::io::ErrorKind::Interrupted => (),
                        _ => {
                            log::warn!("Unexpected read error: {}.", err);
                            return Ok(());
                        }
                    }
                }
            }
        }
    }

    pub fn peer_write(&mut self) -> Result<(), crate::Error> {
        loop {
            if self.write_buffer.is_empty() {
                return Ok(());
            }
            match std::io::Write::write(&mut self.peer, &self.write_buffer[..]) {
                Ok(0) => {
                    log::warn!("Wrote 0 to peer.");
                }
                Ok(amt) => {
                    log::trace!("Wrote {}.", amt);
                    self.write_buffer.drain(..amt);
                }
                Err(err) => match err.kind() {
                    std::io::ErrorKind::WouldBlock => {
                        return Ok(());
                    }
                    std::io::ErrorKind::Interrupted => (),
                    _ => {
                        log::warn!("Unexpected read error: {}.", err);
                        return Ok(());
                    }
                },
            }
        }
    }

    pub fn peer_event(&mut self, readiness: mio::Ready) -> Result<(), crate::Error> {
        log::trace!("Peer event: {:?}", readiness);
        if readiness.is_readable() {
            self.peer_read()?;
        }
        if readiness.is_writable() {
            self.peer_write()?;
        }
        self.consume_messages()?;
        Ok(())
    }

    fn consume_handshake(&mut self) -> Result<HandshakeStatus, crate::Error> {
        match self.config.mode.side() {
            crate::config::Side::Server => self.consume_client_hello(),
            crate::config::Side::Client => self.consume_server_hello(),
            crate::config::Side::Other => {
                return Err(crate::Error::ProgramBug);
            }
        }
    }

    fn consume_server_hello(&mut self) -> Result<HandshakeStatus, crate::Error> {
        if self.read_buffer.len() < crate::crypto::secretstream::HEADERBYTES {
            return Ok(HandshakeStatus::Incomplete);
        }
        let serversend_key = match self
            .side_data
            .client_data_mut()?
            .serversend_stream_key
            .take()
        {
            Some(key) => key,
            None => {
                log::error!("Consuming client hello but no serversend_stream_key is available. This shouldn't be possible.");
                return Err(crate::Error::ProgramBug);
            }
        };
        let header = match crate::crypto::secretstream::Header::from_slice(
            &self.read_buffer[..crate::crypto::secretstream::HEADERBYTES],
        ) {
            Some(x) => x,
            None => {
                log::error!("Failed to instantiate secretstream header.");
                return Err(crate::Error::Crypto);
            }
        };
        let stream = match crate::crypto::secretstream::Stream::init_pull(&header, &serversend_key)
        {
            Ok(x) => x,
            Err(_) => {
                log::error!("Failed to init recv stream.");
                return Err(crate::Error::Crypto);
            }
        };
        self.recv_stream = Some(stream);
        self.read_buffer
            .drain(..crate::crypto::secretstream::HEADERBYTES);
        log::info!("Handshake complete.");

        self.client_setup()?;

        Ok(HandshakeStatus::Complete)
    }

    fn send_message(
        &mut self,
        message_type: crate::message::MessageType,
        data: &[u8],
    ) -> Result<MessageId, crate::Error> {
        let len = match data.len().checked_add(2) {
            Some(x) => x,
            None => {
                log::error!("Too much message data");
                return Err(crate::Error::ProgramBug);
            }
        };
        self.message_construction_buffer.resize(len, 0);
        self.message_construction_buffer[2..].copy_from_slice(&data);
        self.send_message_inplace(message_type)
    }

    fn send_message_inplace(
        &mut self,
        message_type: crate::message::MessageType,
    ) -> Result<MessageId, crate::Error> {
        let message_type_bytes = match num_traits::ToPrimitive::to_u16(&message_type) {
            Some(x) => x,
            None => {
                return Err(crate::Error::ProgramBug);
            }
        }
        .to_be_bytes();
        self.message_construction_buffer[..2].copy_from_slice(&message_type_bytes);
        let len = match self.message_construction_buffer.len().checked_sub(2) {
            Some(x) => x,
            None => {
                log::error!("Tried to send_message_inplace with less than 2 bytes of data");
                return Err(crate::Error::ProgramBug);
            }
        };
        let len = match <u16 as std::convert::TryFrom<_>>::try_from(len) {
            Ok(x) => x,
            Err(_) => {
                log::error!("Tried to send a messagee with {} bytes of data.", len);
                return Err(crate::Error::OversizedMessage);
            }
        };
        let send_length_nonce = match self.send_length_nonce {
            Some(ref mut x) => x,
            None => {
                return Err(crate::Error::ProgramBug);
            }
        };
        let send_length_key = match self.send_length_key {
            Some(ref x) => x,
            None => {
                return Err(crate::Error::ProgramBug);
            }
        };
        let mut len = len.to_be_bytes();
        crate::crypto::stream::stream_xor_inplace(
            &mut len[..],
            &send_length_nonce,
            &send_length_key,
        );
        send_length_nonce.increment_le_inplace();
        self.write_buffer.extend(&len[..]);
        let send_stream = match self.send_stream {
            Some(ref mut x) => x,
            None => {
                return Err(crate::Error::ProgramBug);
            }
        };
        let encrypted_message = match send_stream.push(
            &self.message_construction_buffer,
            None,
            crate::crypto::secretstream::Tag::Message,
        ) {
            Ok(data) => data,
            Err(_) => {
                return Err(crate::Error::Crypto);
            }
        };
        self.write_buffer.extend(&encrypted_message);
        let message_id = MessageId(self.send_message_ticker, MessageDirection::Sent);
        self.send_message_ticker = match self.send_message_ticker.checked_add(1) {
            Some(x) => x,
            None => {
                return Err(crate::Error::Overflow);
            }
        };
        Ok(message_id)
    }

    fn send_message_cbor<T>(&mut self, payload: &T) -> Result<MessageId, crate::Error>
    where
        T: serde::Serialize + crate::message::TypeTagged,
    {
        let serialized = match serde_cbor::to_vec(&payload) {
            Ok(x) => x,
            Err(err) => {
                log::error!("Serialization error: {}.", err);
                return Err(crate::Error::Cbor(err));
            }
        };
        self.send_message(<T as crate::message::TypeTagged>::MESSAGE_TYPE, &serialized)
    }

    fn into_raw_mode(&mut self) -> Result<(), crate::Error> {
        let tty_file = match termion::get_tty() {
            Ok(x) => x,
            Err(err) => {
                log::error!("Failed to get tty: {}.", err);
                return Err(crate::Error::Tty);
            }
        };
        self.side_data.client_data_mut()?.raw_mode_holder =
            match termion::raw::IntoRawMode::into_raw_mode(tty_file) {
                Ok(x) => Some(x),
                Err(err) => {
                    log::error!("Failed to set raw mode: {}.", err);
                    return Err(crate::Error::Tty);
                }
            };
        Ok(())
    }

    fn client_setup(&mut self) -> Result<(), crate::Error> {
        if termion::is_tty(&std::io::stdin()) {
            let tty_token = crate::token::Token::new(
                crate::token::Category::Single,
                crate::token::Single::Tty,
            )?;
            match self.poll.register(
                &mio::unix::EventedFd(&libc::STDIN_FILENO),
                mio::Token::from(tty_token),
                mio::Ready::readable(),
                mio::PollOpt::level(),
            ) {
                Ok(_) => (),
                Err(err) => {
                    return Err(crate::Error::Mio(err));
                }
            }
            let (cols, rows) = match termion::terminal_size() {
                Ok(x) => x,
                Err(_) => (80, 24),
            };
            self.into_raw_mode()?;
            let mut tty_request = crate::message::TtyRequest::default();
            tty_request.cols = cols;
            tty_request.rows = rows;
            self.send_message_cbor(&tty_request)?;
        } else {
            let simple_command = crate::message::SimpleCommand {
                program: "/bin/sh".to_string(),
                args: vec![
                    "-c".to_string(),
                    r#"echo -e -n '\x00\x88\xffasdf'"#.to_string(),
                ],
            };
            self.send_message_cbor(&simple_command)?;
        }

        log::trace!("Client setup complete.");
        Ok(())
    }

    fn consume_client_hello(&mut self) -> Result<HandshakeStatus, crate::Error> {
        let width = <crate::message::ClientHello as bytepack::BytePack>::WIDTH;
        if self.read_buffer.len() < width {
            return Ok(HandshakeStatus::Incomplete);
        }
        let mut client_hello: crate::message::ClientHello =
            bytepack::BytePack::from_slice(&self.read_buffer[..width]);
        self.read_buffer.drain(..width);
        log::trace!("Got ClientHello.");

        let nonce = crate::crypto::secretbox::Nonce(client_hello.symmetric_nonce);
        let mac = crate::crypto::secretbox::Tag(client_hello.symmetric_mac);
        let symmetric_key = match self.config.symmetric_key {
            Some(ref x) => x,
            None => {
                log::error!("No symmetric key provided.");
                return Err(crate::Error::MissingConfigOption);
            }
        };
        match crate::crypto::secretbox::open_detached(
            &mut client_hello.symmetric_ciphertext[..],
            &mac,
            &nonce,
            symmetric_key,
        ) {
            Ok(_) => {
                log::trace!("Successfully decrypted ClientHello secretbox.");
            }
            Err(_) => {
                log::error!("Failed to decrypt ClientHello secretbox");
                return Err(crate::Error::Crypto);
            }
        };

        let inner1: crate::message::ClientHelloInner1 =
            bytepack::BytePack::from_slice(&client_hello.symmetric_ciphertext);
        let sk = match self.config.server_secret_key {
            Some(ref x) => x,
            None => {
                log::error!("No secret key available.");
                return Err(crate::Error::MissingConfigOption);
            }
        };
        let pk = sk.public_key();
        let inner2_bytes =
            match crate::crypto::sealedbox::open(&inner1.asymmetric_ciphertext, &pk, sk) {
                Ok(x) => x,
                Err(_) => {
                    log::error!("Failed to decrypt ClientHello sealedbox.");
                    return Err(crate::Error::Crypto);
                }
            };
        log::trace!("Successfully decrypted ClientHello sealedbox.");

        let mut inner2: crate::message::ClientHelloInner2 =
            bytepack::BytePack::from_slice(&inner2_bytes[..]);
        log::trace!(
            "Advertised client public key: {}",
            data_encoding::BASE32_NOPAD.encode(&inner2.client_pubkey)
        );
        let client_pubkey = crate::crypto::box_::PublicKey(inner2.client_pubkey);
        let mac = crate::crypto::box_::Tag(inner2.asymmetric2_mac);
        let nonce = crate::crypto::box_::Nonce(inner2.asymmetric2_nonce);
        match crate::crypto::box_::open_detached(
            &mut inner2.asymmetric2_ciphertext,
            &mac,
            &nonce,
            &client_pubkey,
            &sk,
        ) {
            Ok(_) => {
                log::trace!("Sucessfully decrypted ClientHello box_.");
            }
            Err(_) => {
                log::error!("Failed to decrypt ClientHello box_.");
                return Err(crate::Error::Crypto);
            }
        };

        let inner3: crate::message::ClientHelloInner3 =
            bytepack::BytePack::from_slice(&inner2.asymmetric2_ciphertext);
        let clientsend_stream_header =
            crate::crypto::secretstream::Header(inner3.clientsend_stream_header);
        let clientsend_stream_key = crate::crypto::secretstream::Key(inner3.clientsend_stream_key);
        let recv_stream = match crate::crypto::secretstream::Stream::init_pull(
            &clientsend_stream_header,
            &clientsend_stream_key,
        ) {
            Ok(x) => x,
            Err(_) => {
                log::error!("clientsend secretstream initialization failed.");
                return Err(crate::Error::Crypto);
            }
        };
        log::trace!("clientsend secretstream successfully initialized.");
        self.recv_stream = Some(recv_stream);

        let serversend_stream_key = crate::crypto::secretstream::Key(inner3.serversend_stream_key);
        match crate::crypto::secretstream::Stream::init_push(&serversend_stream_key) {
            Ok((send_stream, header)) => {
                self.write_buffer.extend(&header.0);
                self.send_stream = Some(send_stream);
            }
            Err(_) => {
                return Err(crate::Error::Crypto);
            }
        }
        self.send_length_key = Some(crate::crypto::stream::Key(inner3.serversend_length_key));
        self.send_length_nonce = Some(crate::crypto::stream::Nonce(inner3.serversend_length_nonce));
        self.recv_length_key = Some(crate::crypto::stream::Key(inner3.clientsend_length_key));
        self.recv_length_nonce = Some(crate::crypto::stream::Nonce(inner3.clientsend_length_nonce));

        log::trace!("Handshake complete.");

        Ok(HandshakeStatus::Complete)
    }

    fn consume_decrypted_message(
        &mut self,
        message_id: MessageId,
        message_type: crate::message::MessageType,
        payload: &[u8],
    ) -> Result<(), crate::Error> {
        log::trace!(
            "Consuming message {}: {:?} ({:?}).",
            message_id.0,
            message_type,
            payload
        );
        match message_type {
            crate::message::MessageType::ServerConnectConnectionData
            | crate::message::MessageType::ServerAcceptConnectionData => {
                let orientation = match message_type {
                    crate::message::MessageType::ServerConnectConnectionData => {
                        ConnectionOrientation::ServerConnect
                    }
                    crate::message::MessageType::ServerAcceptConnectionData => {
                        ConnectionOrientation::ServerAccept
                    }
                    _ => {
                        return Err(crate::Error::ProgramBug);
                    }
                };
                let mut read_reference = &payload[..];
                let reference_id: u64 = match serde_cbor::from_reader(&mut read_reference) {
                    Ok(x) => x,
                    Err(err) => {
                        let offset = <usize as std::convert::TryFrom<_>>::try_from(err.offset())?
                            .checked_sub(1)
                            .ok_or(crate::Error::Overflow)?;
                        read_reference = &payload[..offset];
                        let result = serde_cbor::from_reader(&mut read_reference)?;
                        read_reference = &payload[offset..];
                        result
                    }
                };
                log::trace!("Connection data reference id: {}.", reference_id);
                let connection =
                    match self.port_forward_connections.iter_mut().find(|(_, x)| {
                        x.orientation == orientation && x.reference_id == reference_id
                    }) {
                        Some(x) => x.1,
                        None => {
                            return self.reject(message_id);
                        }
                    };
                let iovec = nix::sys::uio::IoVec::from_slice(read_reference);
                nix::sys::socket::sendmsg(
                    std::os::unix::io::AsRawFd::as_raw_fd(&connection.connection),
                    &[iovec][..],
                    &[][..],
                    nix::sys::socket::MsgFlags::MSG_WAITALL,
                    None,
                )?;
                Ok(())
            }
            crate::message::MessageType::TtyRequest => {
                if self.side_data.server_data().is_err() {
                    return self.reject(message_id);
                }
                if self.side_data.server_data()?.pty_fd.is_some() {
                    return self.reject(message_id);
                }
                let payload: crate::message::TtyRequest = match serde_cbor::from_slice(payload) {
                    Ok(x) => x,
                    Err(err) => {
                        log::error!("Malformed message: {:?}", err);
                        return Err(crate::Error::MalformedMessage);
                    }
                };
                let winsize = nix::pty::Winsize {
                    ws_row: payload.rows,
                    ws_col: payload.cols,
                    ws_xpixel: 0,
                    ws_ypixel: 0,
                };
                let (parent_fd, child_fd) = match nix::pty::openpty(Some(&winsize), None) {
                    Ok(nix::pty::OpenptyResult {
                        master: parent,
                        slave: child,
                    }) => (parent, child),
                    Err(err) => {
                        log::error!("Failed to create pty: {}.", err);
                        return Err(crate::Error::Tty);
                    }
                };
                // Hopefully nix exports some safe getpwent stuff soon: https://github.com/nix-rust/nix/pull/864
                // This is just a placeholder shim for now.
                let (program, args) = match (payload.program, payload.args) {
                    (Some(x), y) => (x, y),
                    (None, _) => ("/bin/sh".to_string(), vec!["-sh".to_string()]),
                };
                let c_program = match std::ffi::CString::new(program) {
                    Ok(x) => x,
                    Err(_) => {
                        return self.reject(message_id);
                    }
                };
                let c_args = match args
                    .into_iter()
                    .map(std::ffi::CString::new)
                    .collect::<Result<Vec<std::ffi::CString>, _>>()
                {
                    Ok(x) => x,
                    Err(_) => {
                        return self.reject(message_id);
                    }
                };
                match nix::unistd::fork() {
                    Ok(nix::unistd::ForkResult::Parent { child }) => {
                        log::info!("Created child process {}", child);
                        self.side_data.server_data_mut()?.pty_fd = Some(parent_fd);
                        self.side_data.server_data_mut()?.pty_pid = Some(child);
                        let token = crate::token::Token::new(
                            crate::token::Category::Single,
                            crate::token::Single::ServerTtyChild,
                        )?;
                        let _ = nix::unistd::close(child_fd);
                        match self.poll.register(
                            &mio::unix::EventedFd(&parent_fd),
                            mio::Token::from(token),
                            mio::Ready::readable(),
                            mio::PollOpt::level(),
                        ) {
                            Ok(_) => (),
                            Err(err) => {
                                return Err(crate::Error::Mio(err));
                            }
                        }
                    }
                    Ok(nix::unistd::ForkResult::Child) => {
                        // =============================
                        // ASYNC-SIGNAL-SAFE DANGER ZONE
                        // =============================
                        match nix::unistd::setsid() {
                            Ok(_) => (),
                            Err(_) => {
                                std::process::exit(1);
                            }
                        }
                        #[allow(clippy::identity_conversion)]
                        // The .into() is necessary on BSDs probably?
                        // This is another thing I'd love to do via nix
                        unsafe {
                            libc::ioctl(child_fd, libc::TIOCSCTTY.into(), 0u64)
                        };
                        let _ = nix::unistd::dup2(child_fd, 0);
                        let _ = nix::unistd::dup2(child_fd, 1);
                        let _ = nix::unistd::dup2(child_fd, 2);
                        //for i in 3..=std::i32::MAX {
                        //    let _ = nix::unistd::close(i);
                        //}
                        let _ = nix::unistd::execvp(&c_program, &c_args[..]);
                        std::process::exit(1);
                        // =================================
                        // END ASYNC-SIGNAL-SAFE DANGER ZONE
                        // =================================
                    }
                    Err(err) => {
                        log::error!("Fork failed: {}.", err);
                        return self.reject(message_id);
                    }
                }
                Ok(())
            }
            crate::message::MessageType::TtyData => match self.config.mode.side() {
                crate::config::Side::Client => {
                    let mut tty = match termion::get_tty() {
                        Ok(x) => x,
                        Err(err) => {
                            log::error!("Failed to get TTY: {}.", err);
                            return Ok(());
                        }
                    };
                    let _ = std::io::Write::write_all(&mut tty, &payload);
                    let _ = std::io::Write::flush(&mut tty);
                    Ok(())
                }
                crate::config::Side::Server => {
                    let tty_fd = match self.side_data.server_data()?.pty_fd {
                        Some(x) => x,
                        None => {
                            log::trace!("Got TtyData but don't have a TTY. Ignoring.");
                            return Ok(());
                        }
                    };
                    let _ = nix::unistd::write(tty_fd, &payload[..]);
                    let _ = nix::unistd::fsync(tty_fd);
                    Ok(())
                }
                crate::config::Side::Other => {
                    return Err(crate::Error::ProgramBug);
                }
            },
            crate::message::MessageType::TtyResize => match self.config.mode.side() {
                crate::config::Side::Server => {
                    let pty_fd = match self.side_data.server_data()?.pty_fd {
                        Some(x) => x,
                        None => {
                            return self.reject(message_id);
                        }
                    };
                    let pty_pid = match self.side_data.server_data()?.pty_pid {
                        Some(x) => x,
                        None => {
                            return self.reject(message_id);
                        }
                    };
                    let payload: crate::message::TtyResize = serde_cbor::from_slice(&payload)?;
                    log::trace!("TtyResize: {:?}", payload);
                    let winsize = libc::winsize {
                        ws_row: payload.rows,
                        ws_col: payload.cols,
                        ws_xpixel: 0,
                        ws_ypixel: 0,
                    };
                    unsafe { libc::ioctl(pty_fd, libc::TIOCSWINSZ.into(), &winsize as *const _) };
                    nix::sys::signal::kill(pty_pid, nix::sys::signal::Signal::SIGWINCH)?;
                    Ok(())
                }
                _ => self.reject(message_id),
            },
            crate::message::MessageType::BindRequest => match self.config.mode.side() {
                crate::config::Side::Server => {
                    let payload: crate::message::BindRequest = serde_cbor::from_slice(payload)?;
                    let bind = match mio::net::TcpListener::bind(&payload.bind_spec) {
                        Ok(x) => x,
                        Err(err) => {
                            log::trace!("Bind failed: {}.", err);
                            return self.reject(message_id);
                        }
                    };
                    let slot = crate::token::FindSlot::find_slot(&self.port_forward_binds)
                        .ok_or(crate::Error::Overflow)?;

                    let token = match crate::token::Token::new(
                        crate::token::Category::PortForwardBind,
                        slot,
                    ) {
                        Ok(x) => x,
                        Err(_err) => {
                            return self.reject(message_id);
                        }
                    };
                    self.poll
                        .register(
                            &bind,
                            mio::Token::from(token),
                            mio::Ready::readable(),
                            mio::PollOpt::edge(),
                        )
                        .map_err(|x| crate::Error::Mio(x))?;

                    self.port_forward_binds.insert(
                        slot,
                        PortForwardBind {
                            bind,
                            connect_destination: None,
                            reference_id: Some(message_id.0),
                        },
                    );
                    Ok(())
                }
                _ => self.reject(message_id),
            },
            crate::message::MessageType::BindConnection => match self.config.mode.side() {
                crate::config::Side::Client => {
                    let payload: crate::message::BindConnection = serde_cbor::from_slice(payload)?;
                    let reference = match self
                        .message_id_reference
                        .get(&MessageId(payload.bind_reference, MessageDirection::Sent))
                    {
                        Some(MessageReference::RemoteBind(x)) => x,
                        _ => {
                            return self.reject(message_id);
                        }
                    };
                    let connection = std::net::TcpStream::connect(&reference.connect_destination)?;
                    let connection = mio::net::TcpStream::from_stream(connection)
                        .map_err(|x| crate::Error::Mio(x))?;
                    let slot = crate::token::FindSlot::find_slot(&self.port_forward_connections)
                        .ok_or(crate::Error::Overflow)?;
                    let token = crate::token::Token::new(
                        crate::token::Category::PortForwardConnection,
                        slot,
                    )?;
                    self.poll
                        .register(
                            &connection,
                            mio::Token::from(token),
                            mio::Ready::readable(),
                            mio::PollOpt::level(),
                        )
                        .map_err(|x| crate::Error::Mio(x))?;
                    let entry = PortForwardConnection {
                        connection,
                        orientation: ConnectionOrientation::ServerAccept,
                        reference_id: message_id.0,
                    };
                    self.port_forward_connections.insert(slot, entry);
                    Ok(())
                }
                _ => self.reject(message_id),
            },
            crate::message::MessageType::ConnectRequest => match self.config.mode.side() {
                crate::config::Side::Server => {
                    let payload: crate::message::ConnectRequest = serde_cbor::from_slice(payload)?;
                    let slot = crate::token::FindSlot::find_slot(&self.port_forward_connections)
                        .ok_or(crate::Error::Overflow)?;
                    let connection = match std::net::TcpStream::connect(&payload.destination) {
                        Ok(x) => x,
                        Err(err) => {
                            log::trace!("Error while connecting port forward connection: {}.", err);
                            return self.reject(message_id);
                        }
                    };
                    log::trace!("Connection established to {:?}.", connection.peer_addr());
                    let connection = mio::net::TcpStream::from_stream(connection)
                        .map_err(|x| crate::Error::Mio(x))?;
                    let token = crate::token::Token::new(
                        crate::token::Category::PortForwardConnection,
                        slot,
                    )?;
                    self.poll
                        .register(
                            &connection,
                            mio::Token::from(token),
                            mio::Ready::readable(),
                            mio::PollOpt::edge(),
                        )
                        .map_err(|x| crate::Error::Mio(x))?;
                    let entry = PortForwardConnection {
                        connection,
                        reference_id: message_id.0,
                        orientation: ConnectionOrientation::ServerConnect,
                    };
                    self.port_forward_connections.insert(slot, entry);

                    Ok(())
                }
                _ => self.reject(message_id),
            },
            _ => Ok(()),
        }
    }

    fn reject(&mut self, message_id: MessageId) -> Result<(), crate::Error> {
        self.send_message_cbor(&crate::message::Reject(message_id.0))?;
        Ok(())
    }

    fn consume_messages(&mut self) -> Result<(), crate::Error> {
        loop {
            match self.consume_message()? {
                ConsumeMessageStatus::Finished => {
                    break;
                }
                ConsumeMessageStatus::NotFinished => {
                    //
                }
            }
        }
        Ok(())
    }

    fn consume_message(&mut self) -> Result<ConsumeMessageStatus, crate::Error> {
        if self.read_buffer.len() <= std::mem::size_of::<u16>() {
            return Ok(ConsumeMessageStatus::Finished);
        }
        let mut len = [0u8; 2];
        len.copy_from_slice(&self.read_buffer[..2]);
        let recv_length_nonce = match self.recv_length_nonce {
            Some(ref mut x) => x,
            None => {
                return Err(crate::Error::ProgramBug);
            }
        };
        let recv_length_key = match self.recv_length_key {
            Some(ref x) => x,
            None => {
                return Err(crate::Error::ProgramBug);
            }
        };
        crate::crypto::stream::stream_xor_inplace(&mut len[..], recv_length_nonce, recv_length_key);
        let len = usize::from(u16::from_be_bytes(len));
        log::trace!("Looking at a message length tag of {} bytes.", len,);
        let required_buffer_len = match std::mem::size_of::<u16>()
            .checked_add(crate::crypto::secretstream::ABYTES)
            .and_then(|x| x.checked_add(std::mem::size_of::<u16>()))
            .and_then(|x| x.checked_add(len))
        {
            Some(x) => x,
            None => {
                return Err(crate::Error::ProgramBug);
            }
        };
        log::trace!(
            "I would need {} bytes in my read buffer to be able to receive a full message.",
            required_buffer_len
        );
        if self.read_buffer.len() < required_buffer_len {
            log::trace!(
                "I only have {}, so I can't process the message yet.",
                self.read_buffer.len()
            );
            return Ok(ConsumeMessageStatus::Finished);
        }
        log::trace!(
            "I have {} available. Proceeding to process the message.",
            self.read_buffer.len()
        );
        recv_length_nonce.increment_le_inplace();
        let message_id = MessageId(self.recv_message_ticker, MessageDirection::Recieved);
        self.recv_message_ticker = match self.recv_message_ticker.checked_add(1) {
            Some(x) => x,
            None => {
                log::error!("Too many messages.");
                return Err(crate::Error::Overflow);
            }
        };
        let recv_stream = match self.recv_stream {
            Some(ref mut x) => x,
            None => {
                return Err(crate::Error::ProgramBug);
            }
        };
        let message_body = &self.read_buffer[..required_buffer_len][2..];
        log::trace!("Encrypted message body: {:?}.", message_body);
        let (data, tag) = match recv_stream.pull(message_body, None) {
            Ok(x) => x,
            Err(_) => {
                log::error!("Failed to decrypt message.");
                return Err(crate::Error::Crypto);
            }
        };
        log::trace!("Decrypted message. Tag: {:?}.", tag);
        self.read_buffer.drain(..required_buffer_len);
        let mut message_type = [0u8; 2];
        message_type.copy_from_slice(&data[..2]);
        let message_type = u16::from_be_bytes(message_type);
        let message_type = match num_traits::FromPrimitive::from_u16(message_type) {
            Some(x) => x,
            None => {
                return Err(crate::Error::MalformedMessage);
            }
        };
        log::trace!("Message type: {:?}", message_type);
        self.consume_decrypted_message(message_id, message_type, &data[2..])?;
        Ok(ConsumeMessageStatus::NotFinished)
    }

    fn handle_handshake_event(
        &mut self,
        event: mio::Event,
    ) -> Result<HandshakeStatus, crate::Error> {
        if crate::token::Token::from(event.token())
            != crate::token::Token::new(crate::token::Category::Single, crate::token::Single::Peer)?
        {
            return match self.handle_event(event) {
                Ok(()) => Ok(HandshakeStatus::Incomplete),
                Err(err) => Err(err),
            };
        }
        if event.readiness().is_readable() {
            self.peer_read()?;
        }
        if event.readiness().is_writable() {
            self.peer_write()?;
        }
        let result = self.consume_handshake();
        self.reregister_peer()?;
        result
    }

    fn server_tty_child_event(&mut self) -> Result<(), crate::Error> {
        let mut read_idx = 2;
        self.message_construction_buffer.resize(8192, 0);
        let pty_fd = match self.side_data.server_data()?.pty_fd {
            Some(x) => x,
            None => {
                return Err(crate::Error::ProgramBug);
            }
        };
        match nix::unistd::read(pty_fd, &mut self.message_construction_buffer[read_idx..]) {
            Ok(x) => {
                log::trace!("Read {} from TTY.", x);
                read_idx = match read_idx.checked_add(x) {
                    Some(x) => x,
                    None => {
                        let message = "Read more data than fits in a usize.";
                        log::error!("{}", message);
                        panic!(message);
                    }
                };
            }
            Err(err) => match err {
                nix::Error::Sys(nix::errno::Errno::EIO) => {
                    log::info!("PTY lost.");
                    match self.poll.deregister(&mio::unix::EventedFd(&pty_fd)) {
                        Ok(_) => (),
                        Err(err) => {
                            return Err(crate::Error::Mio(err));
                        }
                    }
                    match nix::unistd::close(pty_fd) {
                        Ok(_) => (),
                        Err(err) => {
                            log::warn!("Error closing pty: {}.", err);
                        }
                    }
                    self.side_data.server_data_mut()?.pty_fd = None;
                    return Ok(());
                }
                err => {
                    log::warn!("Unexpected read error: {}.", err);
                }
            },
        }
        log::trace!("Extracted {}-2 bytes of tty data", read_idx);
        if read_idx > 2 {
            self.message_construction_buffer.truncate(read_idx);
            self.send_message_inplace(crate::message::MessageType::TtyData)?;
        }
        Ok(())
    }

    fn client_tty_event(&mut self) -> Result<(), crate::Error> {
        match self.side_data.client_data()?.input_mode {
            InputMode::Raw => self.client_tty_raw_event(),
            InputMode::Linefeed => self.client_tty_linefeed_event(),
        }
    }

    fn process_metacommand(&mut self, command: &str) -> Result<(), crate::Error> {
        let command_parts = match shlex::split(command) {
            Some(x) => x,
            None => {
                log::error!("Invalid command.");
                return Ok(());
            }
        };
        let metacommand =
            match <crate::metacommand::Metacommand as structopt::StructOpt>::from_iter_safe(
                command_parts,
            ) {
                Ok(x) => x,
                Err(err) => {
                    match err.kind {
                        structopt::clap::ErrorKind::HelpDisplayed => {
                            let help = err.to_string().replace('\n', "\r\n");
                            print!("{}", help);
                        }
                        structopt::clap::ErrorKind::VersionDisplayed => {
                            let version = err.to_string().replace('\n', "\r\n");
                            print!("{}", version);
                        }
                        _ => {
                            log::error!("Invalid command: {}.", err);
                        }
                    }
                    return Ok(());
                }
            };
        log::trace!("Metacommand result: {:?}", metacommand);
        match metacommand {
            crate::metacommand::Metacommand::RemotePortForward {
                bind_spec,
                connect_destination,
            } => {
                let reference_id =
                    self.send_message_cbor(&crate::message::BindRequest { bind_spec })?;
                self.message_id_reference.insert(
                    reference_id,
                    MessageReference::RemoteBind(RemoteBindReference {
                        connect_destination,
                    }),
                );
            }
            crate::metacommand::Metacommand::LocalPortForward {
                bind_spec,
                connect_destination,
            } => {
                let idx = match crate::token::FindSlot::find_slot(&self.port_forward_binds) {
                    Some(x) => x,
                    None => {
                        log::error!("Too many local portforwards.");
                        return Ok(());
                    }
                };
                let listener = match mio::net::TcpListener::bind(&bind_spec) {
                    Ok(x) => x,
                    Err(err) => {
                        log::error!("Bind error: {}.", err);
                        return Ok(());
                    }
                };
                let token = crate::token::Token::new(crate::token::Category::PortForwardBind, idx)?;
                self.poll
                    .register(
                        &listener,
                        mio::Token::from(token),
                        mio::Ready::readable(),
                        mio::PollOpt::level(),
                    )
                    .map_err(|x| crate::Error::Mio(x))?;
                self.port_forward_binds.insert(
                    idx,
                    PortForwardBind {
                        bind: listener,
                        reference_id: None,
                        connect_destination: Some(connect_destination),
                    },
                );
            }
            _ => (),
        }
        Ok(())
    }

    fn client_tty_linefeed_event(&mut self) -> Result<(), crate::Error> {
        match self.side_data.client_data()?.linefeed {
            Some(ref interface) => {
                match interface.read_line_step(Some(std::time::Duration::from_secs(0))) {
                    Ok(Some(linefeed::ReadResult::Input(line))) => {
                        match self.side_data.client_data_mut()?.raw_mode_holder {
                            Some(ref mut x) => {
                                let _ = std::io::Write::write(x, &b"\r"[..]);
                                let _ = std::io::Write::flush(x);
                            }
                            None => (),
                        }
                        self.process_metacommand(&line)?;
                        self.side_data.client_data_mut()?.input_mode = InputMode::Raw;
                    }
                    Ok(None) => {
                        //
                    }
                    other => {
                        log::trace!("Unexpected linefeed result: {:?}", other);
                    }
                }
            }
            None => {
                log::warn!("No linefeed available.");
                return Err(crate::Error::Tty);
            }
        }
        Ok(())
    }

    fn client_tty_raw_event(&mut self) -> Result<(), crate::Error> {
        let mut read_idx = 2usize;
        self.message_construction_buffer.resize(8192, 0);
        let mut tty = match termion::get_tty() {
            Ok(x) => x,
            Err(err) => {
                log::error!("Error getting tty: {}.", err);
                return Err(crate::Error::Tty);
            }
        };
        match std::io::Read::read(&mut tty, &mut self.message_construction_buffer[2..]) {
            Ok(x) => {
                log::trace!("Read {} from TTY.", x);
                read_idx = match read_idx.checked_add(x) {
                    Some(x) => x,
                    None => {
                        let message = "Read more data than fits in a usize.";
                        log::error!("{}", message);
                        panic!(message);
                    }
                };
            }
            Err(err) => {
                log::warn!("Unexpected read error: {}.", err);
            }
        }

        if read_idx == 2 {
            // No substantive data.
            return Ok(());
        }

        self.message_construction_buffer.truncate(read_idx);
        if &self.message_construction_buffer[2..]
            == &self.side_data.client_data()?.escape_sequence[..]
        {
            log::trace!("Escape sequence detected. Activating metacommand prompt.");
            match self.side_data.client_data_mut()?.raw_mode_holder {
                Some(ref mut x) => {
                    let _ = std::io::Write::write(x, &b"\r\n\x1b[K"[..]);
                    let _ = std::io::Write::flush(x);
                }
                None => (),
            }
            self.side_data.client_data_mut()?.input_mode = InputMode::Linefeed;
            return self.client_tty_linefeed_event();
        }
        if &self.message_construction_buffer[2..]
            == &self.side_data.client_data()?.exit_sequence[..]
        {
            log::trace!("Exit requested.");
            return Err(crate::Error::ExitRequested);
        }

        self.send_message_inplace(crate::message::MessageType::TtyData)?;

        Ok(())
    }

    fn signal_event(&mut self) -> Result<(), crate::Error> {
        let signals: Vec<_> = self.signals.pending().collect();
        for signal in signals {
            log::trace!("Got signal {}.", signal);
            match signal {
                signal_hook::SIGWINCH => match self.config.mode.side() {
                    crate::config::Side::Client => {
                        let (cols, rows) = termion::terminal_size()?;
                        let message = crate::message::TtyResize { cols, rows };
                        self.send_message_cbor(&message)?;
                    }
                    _ => {
                        log::trace!("Recieved SIGWINCH but I am not a client.");
                    }
                },
                _ => (),
            }
        }
        Ok(())
    }

    fn port_forward_bind_event(&mut self, idx: usize) -> Result<(), crate::Error> {
        loop {
            let bind = match self.port_forward_binds.get_mut(&idx) {
                Some(x) => x,
                None => {
                    return Err(crate::Error::InvalidEvent);
                }
            };
            let (connection, addr) = match bind.bind.accept() {
                Ok(x) => x,
                Err(err) => {
                    if err.kind() == std::io::ErrorKind::WouldBlock {
                        break;
                    }
                    log::error!("Unexpected accept error: {}.", err);
                    break;
                }
            };
            log::trace!(
                "Accepted port forward connection at {:?} from {}.",
                bind.bind.local_addr(),
                addr
            );

            let connection_idx =
                match crate::token::FindSlot::find_slot(&self.port_forward_connections) {
                    Some(x) => x,
                    None => {
                        return Err(crate::Error::Overflow);
                    }
                };
            log::trace!("Using connection slot {}.", connection_idx);
            let token = crate::token::Token::new(
                crate::token::Category::PortForwardConnection,
                connection_idx,
            )?;
            self.poll
                .register(
                    &connection,
                    mio::Token::from(token),
                    mio::Ready::readable(),
                    mio::PollOpt::level(),
                )
                .map_err(|x| crate::Error::Mio(x))?;
            log::trace!("Registered connection to poll.");

            match self.config.mode.side() {
                crate::config::Side::Client => {
                    let destination = match bind.connect_destination {
                        Some(ref x) => x.clone(),
                        None => {
                            return Err(crate::Error::ProgramBug);
                        }
                    };
                    let message = crate::message::ConnectRequest { destination };
                    let reference_id = self.send_message_cbor(&message)?.0;
                    let entry = PortForwardConnection {
                        reference_id,
                        connection,
                        orientation: ConnectionOrientation::ServerConnect,
                    };
                    self.port_forward_connections.insert(connection_idx, entry);
                }
                crate::config::Side::Server => {
                    let bind_reference = match bind.reference_id {
                        Some(x) => x,
                        None => {
                            return Err(crate::Error::ProgramBug);
                        }
                    };
                    let peer_addr = connection.peer_addr()?;
                    let message = crate::message::BindConnection {
                        bind_reference,
                        peer_addr,
                    };
                    let reference_id = self.send_message_cbor(&message)?.0;
                    let entry = PortForwardConnection {
                        reference_id,
                        connection,
                        orientation: ConnectionOrientation::ServerAccept,
                    };
                    self.port_forward_connections.insert(connection_idx, entry);
                }
                _ => {
                    return Err(crate::Error::ProgramBug);
                }
            }
        }
        Ok(())
    }

    fn port_forward_connection_event(&mut self, idx: usize) -> Result<(), crate::Error> {
        log::trace!("Looking up connection by idx {}.", idx);
        let connection = self
            .port_forward_connections
            .get_mut(&idx)
            .ok_or(crate::Error::InvalidEvent)?;
        log::trace!("Connection found.");
        let reference_id = connection.reference_id;
        let mut read_idx = 2usize;
        let mut connection_lost = false;
        self.message_construction_buffer.resize(8192, 0);
        let mut write_reference = &mut self.message_construction_buffer[read_idx..];
        let cbor_before_len = write_reference.len();
        serde_cbor::to_writer(&mut write_reference, &reference_id)?;
        let cbor_len = cbor_before_len
            .checked_sub(write_reference.len())
            .ok_or(crate::Error::Overflow)?;
        log::trace!("Reference idx serialized. CBOR len: {}.", cbor_len);
        read_idx = read_idx
            .checked_add(cbor_len)
            .ok_or(crate::Error::Overflow)?;
        log::trace!("Starting read loop.");
        loop {
            if self.message_construction_buffer[read_idx..].len() == 0 {
                break;
            }
            match std::io::Read::read(
                &mut connection.connection,
                &mut self.message_construction_buffer[read_idx..],
            ) {
                Ok(0) => {
                    log::trace!("Port forward connection lost.");
                    connection_lost = true;
                    break;
                }
                Ok(amt) => {
                    log::trace!("Read {} from port forward connection.", amt);
                    read_idx = read_idx.checked_add(amt).ok_or(crate::Error::Overflow)?;
                }
                Err(err) if err.kind() == std::io::ErrorKind::WouldBlock => {
                    break;
                }
                Err(err) => {
                    log::trace!("Unexpected read error: {}.", err);
                    return Err(crate::Error::from(err));
                }
            }
        }

        self.message_construction_buffer.truncate(read_idx);

        if read_idx > 2 {
            let message_type = match connection.orientation {
                ConnectionOrientation::ServerAccept => {
                    crate::message::MessageType::ServerAcceptConnectionData
                }
                ConnectionOrientation::ServerConnect => {
                    crate::message::MessageType::ServerConnectConnectionData
                }
            };
            self.send_message_inplace(message_type)?;
        }

        if connection_lost {
            let connection = self
                .port_forward_connections
                .remove(&idx)
                .ok_or(crate::Error::ProgramBug)?;
            self.poll
                .deregister(&connection.connection)
                .map_err(|x| crate::Error::Mio(x))?;
            self.send_message_cbor(&crate::message::CloseConnect {
                reference: reference_id,
            })?;
        }
        Ok(())
    }

    pub fn handle_event(&mut self, event: mio::Event) -> Result<(), crate::Error> {
        let token = crate::token::Token::from(event.token());
        let category = match token.category() {
            Some(category) => category,
            None => {
                return Ok(());
            }
        };
        if log::log_enabled!(log::Level::Trace) {
            match category {
                crate::token::Category::Single => {
                    log::trace!(
                        "Event category: {:?}. Event idx: {:?}.",
                        category,
                        token.single()
                    );
                }
                _ => {
                    log::trace!(
                        "Event category: {:?}. Event idx: {}.",
                        category,
                        token.idx()
                    );
                }
            }
        }
        match category {
            crate::token::Category::Single => {
                let single = match token.single() {
                    Some(x) => x,
                    None => {
                        return Ok(());
                    }
                };
                match single {
                    crate::token::Single::Peer => self.peer_event(event.readiness()),
                    crate::token::Single::Tty => self.client_tty_event(),
                    crate::token::Single::ServerTtyChild => self.server_tty_child_event(),
                    crate::token::Single::Signal => self.signal_event(),
                }
            }
            crate::token::Category::PortForwardBind => {
                let idx = token.idx();
                let _ = self.port_forward_bind_event(idx);
                Ok(())
            }
            crate::token::Category::PortForwardConnection => {
                let idx = token.idx();
                let _ = self.port_forward_connection_event(idx);
                Ok(())
            }
            _ => Ok(()),
        }
    }
}
