#[derive(Debug)]
pub enum Error {
    TokenIdxTooLarge,
    SodiumOxideInitFailed,
    NoBinds,
    NoDestination,
    ConnectFailed,
    InvalidKeyLength,
    Crypto,
    MissingConfigOption,
    ProgramBug,
    OversizedMessage,
    MalformedMessage,
    Overflow,
    Tty,
    NoHome,
    Disabled,
    InvalidEvent,
    Other,
    ResourceLimit,
    ExitRequested,
    IncorrectMode,
    Mio(std::io::Error),
    Decode(data_encoding::DecodeError),
    Cbor(serde_cbor::Error),
    Io(std::io::Error),
    Nix(nix::Error),
    Utf8(std::string::FromUtf8Error),
}

impl std::convert::From<data_encoding::DecodeError> for Error {
    fn from(other: data_encoding::DecodeError) -> Error {
        log::trace!("Converting a data_encoding error: {}.", other);
        Error::Decode(other)
    }
}

impl std::convert::From<std::io::Error> for Error {
    fn from(other: std::io::Error) -> Error {
        log::trace!("Converting an IO error: {}.", other);
        Error::Io(other)
    }
}

impl std::convert::From<nix::Error> for Error {
    fn from(other: nix::Error) -> Error {
        log::trace!("Converting a nix error: {}.", other);
        Error::Nix(other)
    }
}

impl std::convert::From<std::string::FromUtf8Error> for Error {
    fn from(other: std::string::FromUtf8Error) -> Error {
        log::trace!("Converting a UTF-8 error: {}.", other);
        Error::Utf8(other)
    }
}

impl std::convert::From<serde_cbor::error::Error> for Error {
    fn from(other: serde_cbor::error::Error) -> Error {
        log::trace!("Converting a serde_cbor error: {}.", other);
        Error::Cbor(other)
    }
}

impl std::convert::From<std::num::TryFromIntError> for Error {
    fn from(other: std::num::TryFromIntError) -> Error {
        log::trace!("Converting a TryFromIntError: {}.", other);
        Error::Overflow
    }
}

impl std::string::ToString for Error {
    fn to_string(&self) -> String {
        format!("{:?}", self)
    }
}
