fn main() {
    env_logger::init();
    let config = <oxy::config::Config as structopt::StructOpt>::from_args();
    match oxy::run(config) {
        Ok(_) => (),
        Err(err) => {
            log::trace!("Exiting due to {:?}.", err);
            std::process::exit(1)
        }
    }
}
