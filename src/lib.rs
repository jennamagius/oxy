//! An SSH-like remote access server and client.

#![deny(clippy::integer_arithmetic)] // The desired behavior for integer overflow must be explicitly specified at each occasion.
#![deny(clippy::cast_precision_loss)] // Use try_from. If you want to truncate: zero the high bits & then use try_from.
#![allow(clippy::single_match)] // I'd rather use one syntax than two syntaxes

mod accept;
mod connect;
/// Some reexports of specific sodiumoxide algorithm sets to reduce duplication of algorithm set names.
mod crypto;
mod error;
mod metacommand;
mod oxy;
mod reexec;

/// Oxy configuration. The config struct can be used to parse command line options via structopt and configuration files via serde.
pub mod config;
/// Data structures for wire messages.
pub mod message;
/// A scheme for organizing mio::Tokens into categories.
pub mod token;

/// An error type.
pub use error::Error;

pub(crate) fn crypto_init() -> Result<(), crate::Error> {
    match sodiumoxide::init() {
        Ok(_) => Ok(()),
        Err(_) => {
            log::error!("sodiumoxide::init failed");
            Err(crate::Error::SodiumOxideInitFailed)
        }
    }
}

pub fn run(config: crate::config::Config) -> Result<(), crate::Error> {
    crypto_init()?;
    let mut oxy = match config.mode {
        crate::config::Mode::ServeOne => accept::accept_one(config)?,
        crate::config::Mode::ReverseServer => connect::connect(config)?,
        crate::config::Mode::ReverseClient => accept::accept_one(config)?,
        crate::config::Mode::Client => connect::connect(config)?,
        crate::config::Mode::Server => accept::accept_forever(config)?,
        crate::config::Mode::Reexec => reexec::reexec(config)?,
    };
    oxy.run()
}
