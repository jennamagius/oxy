pub(crate) fn reexec(_config: crate::config::Config) -> Result<crate::oxy::Oxy, crate::Error> {
    let mut read_buffer = vec![0u8; 8192];
    let mut cmsg_buffer = nix::cmsg_space!([std::os::unix::io::RawFd; 1]);
    let mut read_idx = 0;
    let mut peer_fd = None;
    loop {
        let read_iovec = nix::sys::uio::IoVec::from_mut_slice(&mut read_buffer[read_idx..]);
        match nix::sys::socket::recvmsg(
            libc::STDIN_FILENO,
            &[read_iovec],
            Some(&mut cmsg_buffer),
            nix::sys::socket::MsgFlags::MSG_DONTWAIT,
        ) {
            Ok(result) => {
                log::trace!("recvmsg result: {:?}", result);
                log::trace!("Reexec read {} bytes.", result.bytes);
                read_idx = read_idx
                    .checked_add(result.bytes)
                    .ok_or(crate::Error::Overflow)?;
                for cmsg in result.cmsgs() {
                    log::trace!("Cmsg: {:?}", cmsg);
                    match cmsg {
                        nix::sys::socket::ControlMessageOwned::ScmRights(fds) => {
                            if fds.len() != 1 {
                                log::trace!("Got too many ScmRights fds.");
                                return Err(crate::Error::Other);
                            }
                            if peer_fd.is_some() {
                                log::trace!("Got an ScmRights fd but I already have one.");
                                return Err(crate::Error::Other);
                            }
                            log::trace!("Got an ScmRights fd.");
                            peer_fd = Some(fds[0]);
                        }
                        _ => {
                            log::trace!("Unrecognized control message: {:?}", cmsg);
                            return Err(crate::Error::Other);
                        }
                    }
                }
                if read_idx >= 4 {
                    let mut config_len_bytes = [0u8; 4];
                    config_len_bytes.copy_from_slice(&read_buffer[..4]);
                    let config_len = <usize as std::convert::TryFrom<_>>::try_from(
                        u32::from_be_bytes(config_len_bytes),
                    )?;
                    let total_len = config_len.checked_add(4).ok_or(crate::Error::Overflow)?;
                    log::trace!(
                        "I have {} bytes and I need {} bytes. I {} have a peer fd.",
                        read_idx,
                        total_len,
                        if peer_fd.is_some() { "do" } else { "don't" }
                    );
                    if read_idx >= total_len && peer_fd.is_some() {
                        let peer_fd = match peer_fd {
                            Some(x) => x,
                            None => {
                                return Err(crate::Error::ProgramBug);
                            }
                        };
                        let new_config = serde_cbor::from_slice(&read_buffer[4..][..config_len])?;
                        log::trace!("Got new config");
                        let peer_socket =
                            unsafe { std::os::unix::io::FromRawFd::from_raw_fd(peer_fd) };
                        let peer_socket = mio::net::TcpStream::from_stream(peer_socket)
                            .map_err(|x| crate::Error::Mio(x))?;
                        let oxy = crate::oxy::Oxy::new(new_config, peer_socket)?;
                        return Ok(oxy);
                    }
                    read_buffer.resize(total_len.checked_add(1).ok_or(crate::Error::Overflow)?, 0);
                }
            }
            Err(err) => {
                log::trace!("Error during reexec recvmsg: {}.", err);
                match err {
                    nix::Error::Sys(nix::errno::EWOULDBLOCK) => {
                        std::thread::yield_now();
                    }
                    _ => {
                        return Err(crate::Error::Nix(err));
                    }
                }
            }
        }
    }
}
